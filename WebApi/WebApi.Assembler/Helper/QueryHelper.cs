﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApi.Assembler.Helper
{
    public class QueryHelper
    {
        public string ConvertNullValues(string value)
        {
            return string.IsNullOrEmpty(value) ? "NULL" : string.Format("'{0}'", value.Replace("'", "''"));
        }

        public string ConvertNullValues(int? value) {
            return value.HasValue ? value.Value.ToString() : "NULL"; 
        }
        public string ConvertNullValues(float? value)
        {
            return value.HasValue ? value.Value.ToString() : "NULL";
        }

        public string ConvertDateNullValues(DateTime? value)
        {
            return value.HasValue ?  string.Format("TO_DATE('{0}','DD/MM/YYYY HH24:MI:SS')", value.Value.ToString()): "NULL";
        }
    }
}
