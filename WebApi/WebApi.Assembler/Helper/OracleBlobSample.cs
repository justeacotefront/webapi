﻿using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;

namespace WebApi.Assembler.Helper
{
    public class OracleBlobSample
    {
        private const string _connectionString = "User Id=APP_DEV;Password=appDev2018!;Data Source=54.38.230.175/orcldev";

        /// <summary>
        /// Méthode permettant de convertir un objet de type OracleBlob  en objet de type byte[]
        /// </summary>
        /// <param name="blob"></param>
        /// <returns></returns>
        public byte[] ConvertBlobToByte(OracleBlob blob)
        {
            return blob.Value;
        }

        /// <summary>
        /// Méthode permettant de convertir un objet de type byte[] en un objet de type OracleBlob
        /// </summary>
        /// <param name="byteArray"></param>
        /// <returns></returns>
        public OracleBlob ConvertByteArrayToBlob(byte[] byteArray)
        {
            if (byteArray != null)
            {
                OracleConnection con = new OracleConnection(_connectionString);
                con.Open();
                OracleBlob blob = new OracleBlob(con);

                // Write  bytes from byteArray
                int lenght = byteArray.Length; 
                blob.Write(byteArray, 0, lenght);
                
                blob.Close();
                blob.Dispose();

                con.Close();
                con.Dispose();
                return blob;
            }
            return null;

        }
    }
}
