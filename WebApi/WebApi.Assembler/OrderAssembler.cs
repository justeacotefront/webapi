﻿using System;
using Oracle.ManagedDataAccess.Client;
using WebApi.Transport;

namespace WebApi.Assembler
{
    public class OrderAssembler : BaseAssembler<Order>
    {
        protected override void Populate(OracleDataReader source, Order target)
        {
            if (source.HasRows)
            {
                int colIndex = source.GetOrdinal(OrderSqlTools.OrderTable.Id);
                if (!source.IsDBNull(colIndex))
                    target.Identifiant = Convert.ToInt32(source[OrderSqlTools.OrderTable.Id]);
                colIndex = source.GetOrdinal(OrderSqlTools.OrderTable.IdCustomer);
                if (!source.IsDBNull(colIndex))
                    target.CustomerId = Convert.ToInt32(source[OrderSqlTools.OrderTable.IdCustomer]);
                colIndex = source.GetOrdinal(OrderSqlTools.OrderTable.DateCreation);
                if (!source.IsDBNull(colIndex))
                    target.Creation = Convert.ToDateTime(source[OrderSqlTools.OrderTable.DateCreation]);
                colIndex = source.GetOrdinal(OrderSqlTools.OrderTable.DateModification);
                if (!source.IsDBNull(colIndex))
                    target.Modification = Convert.ToDateTime(source[OrderSqlTools.OrderTable.DateModification]);
                colIndex = source.GetOrdinal(OrderSqlTools.OrderTable.Statut);
                if (!source.IsDBNull(colIndex))
                    target.Status = source[OrderSqlTools.OrderTable.Statut].ToString();
                colIndex = source.GetOrdinal(OrderSqlTools.OrderTable.ShopId);
                if (!source.IsDBNull(colIndex))
                    target.IdShop = Convert.ToInt32(source[OrderSqlTools.OrderTable.ShopId]);
                colIndex = source.GetOrdinal(OrderSqlTools.OrderTable.RecoveryDate);
                if (!source.IsDBNull(colIndex))
                    target.RecoveryDate = Convert.ToDateTime(source[OrderSqlTools.OrderTable.RecoveryDate]);
                colIndex = source.GetOrdinal(OrderSqlTools.OrderTable.Description);
                if (!source.IsDBNull(colIndex))
                    target.Description = source[OrderSqlTools.OrderTable.Description].ToString();
            }
        }
    }
}
