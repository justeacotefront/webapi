﻿using System;
using System.Collections.Generic;
using System.Text;
using Oracle.ManagedDataAccess.Client;
using WebApi.Transport;

namespace WebApi.Assembler
{
    public class ShopCategoryAssembler : BaseAssembler<ShopCategory>
    {
        protected override void Populate(OracleDataReader source, ShopCategory target)
        {
            if (source.HasRows)
            {
                int colIndex = source.GetOrdinal(ShopCategorySqlTools.ShopCategoryTable.CategoryId);
                if (!source.IsDBNull(colIndex))
                    target.CategoryId = Convert.ToInt32(source[ShopCategorySqlTools.ShopCategoryTable.CategoryId]);
                colIndex = source.GetOrdinal(ShopCategorySqlTools.ShopCategoryTable.ShopId);
                if (!source.IsDBNull(colIndex))
                    target.ShopId = Convert.ToInt32(source[ShopCategorySqlTools.ShopCategoryTable.ShopId]);
            }
        }
    }
}
