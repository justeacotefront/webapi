﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApi.Assembler
{
    public static class CommentSqlTools
    {  /// <summary>
       /// Description de la table JC_COMMENT.
       /// </summary>
        public static class CommentTable
        {

            /// <summary>
            /// Nom de ma colonne Id dans la table.
            /// </summary>
            public const string Id = "COMMENT_ID";

            /// <summary>
            /// Nom de ma colonne Id dans la table.
            /// </summary>
            public const string IdCustomer = "CUSTOMER_ID";

            /// <summary>
            /// Nom de ma colonne Id dans la table.
            /// </summary>
            public const string IdProduct = "PRODUCT_ID";

            /// <summary>
            /// Nom de ma colonne Titre dans la table
            /// </summary>
            public const string Titre = "COMMENT_TITLE";

            /// <summary>
            /// Nom de la colonne Description dans la table.
            /// </summary>
            public const string Description = "COMMENT_DESCRIPTION";

            /// <summary>
            /// Nom de la colonne DateCreation dans la table.
            /// </summary>
            public const string DateCreation = "COMMENT_CREATION_DATE";

            /// <summary>
            /// Nom de la colonne DateModification dans la table.
            /// </summary>
            public const string DateModification = "COMMENT_MODIFICATION_DATE";
        }

        private const string SQLQUERY_INSERT_COMMENT_PATTERN = "INSERT INTO JC_COMMENT (CUSTOMER_ID, PRODUCT_ID, COMMENT_TITLE,COMMENT_DESCRIPTION,COMMENT_CREATION_DATE) VALUES ({0},{1},'{2}','{3}', TO_DATE(sysdate))";
        public static string SqlQuery_Insert(int idCustomer, int idProduct, string titre, string description)
        {

            return string.Format(SQLQUERY_INSERT_COMMENT_PATTERN, idCustomer, idProduct,
                string.IsNullOrEmpty(titre) ? "" : titre.Replace("'", "''"),
                string.IsNullOrEmpty(description) ? "" : description.Replace("'", "''"));
        }


        private const string SQLQUERY_SELECT_ALL_COMMENT_PATTERN = "SELECT COMMENT_ID,CUSTOMER_ID, PRODUCT_ID, COMMENT_TITLE,COMMENT_DESCRIPTION,COMMENT_CREATION_DATE, COMMENT_MODIFICATION_DATE FROM JC_COMMENT";
        public static string SqlQuery_SelectAll()
        {
            return SQLQUERY_SELECT_ALL_COMMENT_PATTERN;
        }

        private const string SQLQUERY_SELECT_COMMENT_BY_ID_PATTERN = "SELECT COMMENT_ID,CUSTOMER_ID, PRODUCT_ID, COMMENT_TITLE,COMMENT_DESCRIPTION,COMMENT_CREATION_DATE, COMMENT_MODIFICATION_DATE FROM JC_COMMENT WHERE COMMENT_ID = {0}";
        public static string SqlQuery_SelectById(int id)
        {
            return string.Format(SQLQUERY_SELECT_COMMENT_BY_ID_PATTERN, id);
        }

        private const string SQLQUERY_DELETE_COMMENT_BY_ID_PATTERN = "DELETE FROM JC_COMMENT WHERE COMMENT_ID = {0}";
        public static string SqlQuery_Delete(int id)
        {
            return string.Format(SQLQUERY_DELETE_COMMENT_BY_ID_PATTERN, id);
        }

        private const string SQLQUERY_UPDATE_COMMENT_PATTERN = "UPDATE JC_COMMENT  SET COMMENT_TITLE = '{0}', COMMENT_DESCRIPTION = '{1}', COMMENT_MODIFICATION_DATE=TO_DATE(sysdate) WHERE COMMENT_ID = {2} AND CUSTOMER_ID={3} AND PRODUCT_ID={4}";
        public static string SqlQuery_Update(int id, int idCustomer, int idProduct, string titre, string description)
        {
            return string.Format(SQLQUERY_UPDATE_COMMENT_PATTERN, titre.Replace("'", "''"), 
                description.Replace("'", "''"), id, idCustomer, idProduct);
        }

        private const string SQLQUERY_SELECT_COMMENT_BY_IDPRODUCT_PATTERN = "SELECT COMMENT_ID,CUSTOMER_ID, PRODUCT_ID, COMMENT_TITLE,COMMENT_DESCRIPTION,COMMENT_CREATION_DATE, COMMENT_MODIFICATION_DATE FROM JC_COMMENT WHERE PRODUCT_ID = {0}";
        public static string SqlQuery_SelectByIdProduct(int id)
        {
            return string.Format(SQLQUERY_SELECT_COMMENT_BY_IDPRODUCT_PATTERN, id);
        }

    }
}
