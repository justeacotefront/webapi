﻿using Oracle.ManagedDataAccess.Client;
using System.Collections.Generic;

namespace WebApi.Assembler
{
    public abstract class BaseAssembler<T_Object>
        where T_Object : class, new()
    {
        /// <summary>
        /// populates an object.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        protected abstract void Populate(OracleDataReader source, T_Object target);

        /// <summary>
        /// Converts a list of source object to target objects.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public virtual T_Object ConvertItem(OracleDataReader item)
        {
            if (item == null)
                return default(T_Object);

            T_Object target = new T_Object();
            Populate(item, target);
            return target;
        }

        /// <summary>
        /// Converts a list of source object to target objects.
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public virtual List<T_Object> ConvertList(OracleDataReader items)
        {
            if (items == null || !items.HasRows)
                return new List<T_Object>();

            List<T_Object> result = new List<T_Object>();
            while (items.Read())
            {
                result.Add(ConvertItem(items));
            }
            return result;
        }
    }
}
