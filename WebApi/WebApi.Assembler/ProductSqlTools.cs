﻿using WebApi.Assembler.Helper;

namespace WebApi.Assembler
{
    public static class ProductSqlTools
    {
        /// <summary>
        /// Description de la table JC_PRODUCT.
        /// </summary>
        public static class ProductTable
        {
            /// <summary>
            /// Nom de ma colonne Id dans la table.
            /// </summary>
            public const string Id = "PRODUCT_ID";

            /// <summary>
            /// Nom de la colonne IdShop dans la table.
            /// </summary>
            public const string IdShop = "SHOP_ID";

            /// <summary>
            /// Nom de la colonne Nom dans la table.
            /// </summary>
            public const string Nom = "PRODUCT_NAME";

            /// <summary>
            /// Nom de la colonne Description dans la table.
            /// </summary>
            public const string Description = "PRODUCT_DESCRIPTION";


            /// <summary>
            /// Nom de la colonne Weight dans la table.
            /// </summary>
            public const string Weight = "PRODUCT_WEIGHT";

            /// <summary>
            /// Nom de la colonne WeightUnit dans la table.
            /// </summary>
            public const string WeightUnit = "PRODUCT_WEIGHT_UNIT";

            /// <summary>
            /// Nom de la colonne Lenght dans la table.
            /// </summary>
            public const string Lenght = "PRODUCT_LENGTH";

            /// <summary>
            /// Nom de la colonne LenghtUnit dans la table.
            /// </summary>
            public const string LenghtUnit = "PRODUCT_LENGTH_UNIT";

            /// <summary>
            /// Nom de la colonne Depth dans la table.
            /// </summary>
            public const string Depth = "PRODUCT_DEPTH";

            /// <summary>
            /// Nom de la colonne DepthUnit dans la table.
            /// </summary>
            public const string DepthUnit = "PRODUCT_DEPTH_UNIT";

            /// <summary>
            /// Nom de la colonne Width dans la table.
            /// </summary>
            public const string Width = "PRODUCT_WIDTH";

            /// <summary>
            /// Nom de la colonne WidthUnit dans la table.
            /// </summary>
            public const string WidthUnit = "PRODUCT_WIDTH_UNIT";

            /// <summary>
            /// Nom de la colonne Height dans la table.
            /// </summary>
            public const string Height = "PRODUCT_HEIGHT";

            /// <summary>
            /// Nom de la colonne IdShop dans la table.
            /// </summary>
            public const string HeightUnit = "PRODUCT_HEIGHT_UNIT";

            /// <summary>
            /// Nom de la colonne Prix dans la table.
            /// </summary>
            public const string Prix = "PRODUCT_PRICE";

            /// <summary>
            /// Nom de la colonne Currency dans la table.
            /// </summary>
            public const string Currency = "PRODUCT_CURRENCY";

            /// <summary>
            /// Nom de la colonne Inventory dans la table.
            /// </summary>
            public const string Inventory = "PRODUCT_INVENTORY";

            /// <summary>
            /// Nom de la colonne DateCreation dans la table.
            /// </summary>
            public const string DateCreation = "PRODUCT_CREATION_DATE";

            /// <summary>
            /// Nom de la colonne DateModification dans la table.
            /// </summary>
            public const string DateModification = "PRODUCT_MODIFICATION_DATE";

            /// <summary>
            ///  Nom de la colonne Supprimer dans la table.
            /// </summary>
            public const string Supprimer = "PRODUCT_DELETED";

            /// <summary>
            ///  Nom de la colonne Origin dans la table.
            /// </summary>
            public const string Origin = "PRODUCT_ORIGIN";

            /// <summary>
            /// Nom de ma colonne PRODUCT_PICTURE_ID dans la table.
            /// </summary>
            public const string IdPicture = "PRODUCT_PICTURE_ID";

            /// <summary>
            /// Nom de la colonne PicturePath dans la table
            /// </summary>
            public const string PicturePath = "PICTURE_PATH";
        }

        private const string SQLQUERY_INSERT_PRODUCT_PATTERN = "INSERT INTO JC_PRODUCT " +
            "(SHOP_ID,PRODUCT_NAME,PRODUCT_DESCRIPTION,PRODUCT_WEIGHT,PRODUCT_WEIGHT_UNIT," +
            " PRODUCT_LENGTH,PRODUCT_LENGTH_UNIT,PRODUCT_DEPTH,PRODUCT_DEPTH_UNIT," +
            " PRODUCT_WIDTH,PRODUCT_WIDTH_UNIT,PRODUCT_HEIGHT,PRODUCT_HEIGHT_UNIT," +
            " PRODUCT_PRICE,PRODUCT_CURRENCY, PRODUCT_INVENTORY,PRODUCT_ORIGIN, PICTURE_PATH, PRODUCT_CREATION_DATE) " +
            " VALUES " +
            " ({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15}, {16}, {17},TO_DATE(sysdate))";
        public static string SqlQuery_Insert(int idShop, string name, string description, int? weight, string weightUnit, int? lenght, string lenghtUnit, int? depth, string depthUnit,
           int? width, string widthUnit, int? height, string heightUnit, float? prix, string currency, int? inventory, string origin, string picturePath)
        {
            QueryHelper helper = new QueryHelper();
            return string.Format(SQLQUERY_INSERT_PRODUCT_PATTERN, idShop, helper.ConvertNullValues(name), helper.ConvertNullValues(description), helper.ConvertNullValues(weight), helper.ConvertNullValues(weightUnit),
                helper.ConvertNullValues(lenght), helper.ConvertNullValues(lenghtUnit), helper.ConvertNullValues(depth), helper.ConvertNullValues(depthUnit), helper.ConvertNullValues(width), helper.ConvertNullValues(widthUnit),
                helper.ConvertNullValues(height), helper.ConvertNullValues(heightUnit), helper.ConvertNullValues(prix), helper.ConvertNullValues(currency), helper.ConvertNullValues(inventory), helper.ConvertNullValues(origin), helper.ConvertNullValues(picturePath));
        }

        private const string SQLQUERY_SELECT_ALL_PRODUCT_PATTERN = "SELECT PRODUCT_ID," +
            " SHOP_ID,PRODUCT_NAME,PRODUCT_DESCRIPTION,PRODUCT_WEIGHT,PRODUCT_WEIGHT_UNIT," +
            " PRODUCT_LENGTH,PRODUCT_LENGTH_UNIT,PRODUCT_DEPTH,PRODUCT_DEPTH_UNIT," +
            " PRODUCT_WIDTH,PRODUCT_WIDTH_UNIT,PRODUCT_HEIGHT,PRODUCT_HEIGHT_UNIT,PRODUCT_PRICE,PRODUCT_CURRENCY,PRODUCT_INVENTORY,PRODUCT_CREATION_DATE,PRODUCT_MODIFICATION_DATE, PRODUCT_DELETED,PRODUCT_ORIGIN, PRODUCT_PICTURE_ID, PICTURE_PATH FROM JC_PRODUCT " +
            " WHERE PRODUCT_DELETED =0";
        public static string SqlQuery_SelectAll()
        {
            return SQLQUERY_SELECT_ALL_PRODUCT_PATTERN;
        }

        private const string SQLQUERY_SELECT_PRODUCT_BY_ID_PATTERN = "SELECT PRODUCT_ID," +
            " SHOP_ID,PRODUCT_NAME,PRODUCT_DESCRIPTION,PRODUCT_WEIGHT,PRODUCT_WEIGHT_UNIT," +
            " PRODUCT_LENGTH,PRODUCT_LENGTH_UNIT,PRODUCT_DEPTH,PRODUCT_DEPTH_UNIT," +
            " PRODUCT_WIDTH,PRODUCT_WIDTH_UNIT,PRODUCT_HEIGHT,PRODUCT_HEIGHT_UNIT,PRODUCT_PRICE,PRODUCT_CURRENCY,PRODUCT_INVENTORY,PRODUCT_CREATION_DATE,PRODUCT_MODIFICATION_DATE, PRODUCT_DELETED,PRODUCT_ORIGIN, PRODUCT_PICTURE_ID, PICTURE_PATH FROM JC_PRODUCT WHERE PRODUCT_ID = {0}" +
            " AND PRODUCT_DELETED = 0";
        public static string SqlQuery_SelectById(int id)
        {
            return string.Format(SQLQUERY_SELECT_PRODUCT_BY_ID_PATTERN, id);
        }

        private const string SQLQUERY_SELECT_PRODUCT_BY_IDSHOP_PATTERN = "SELECT PRODUCT_ID," +
        " SHOP_ID,PRODUCT_NAME,PRODUCT_DESCRIPTION,PRODUCT_WEIGHT,PRODUCT_WEIGHT_UNIT," +
        " PRODUCT_LENGTH,PRODUCT_LENGTH_UNIT,PRODUCT_DEPTH,PRODUCT_DEPTH_UNIT," +
        " PRODUCT_WIDTH,PRODUCT_WIDTH_UNIT,PRODUCT_HEIGHT,PRODUCT_HEIGHT_UNIT,PRODUCT_PRICE,PRODUCT_CURRENCY,PRODUCT_INVENTORY,PRODUCT_CREATION_DATE,PRODUCT_MODIFICATION_DATE,PRODUCT_ORIGIN,PRODUCT_DELETED, PRODUCT_PICTURE_ID, PICTURE_PATH  FROM JC_PRODUCT WHERE SHOP_ID = {0}" +
        " AND PRODUCT_DELETED = 0";
        public static string SqlQuery_SelectByIdShop(int id)
        {
            return string.Format(SQLQUERY_SELECT_PRODUCT_BY_IDSHOP_PATTERN, id);
        }
        private const string SQLQUERY_DELETE_PRODUCT_BY_ID_PATTERN = "UPDATE JC_PRODUCT  SET PRODUCT_DELETED=1 , PRODUCT_MODIFICATION_DATE=TO_DATE(sysdate) WHERE PRODUCT_ID = {0}";
        public static string SqlQuery_Delete(int id)
        {
            return string.Format(SQLQUERY_DELETE_PRODUCT_BY_ID_PATTERN, id);
        }

        private const string SQLQUERY_PRODUCT_ORDER_PATTERN = "UPDATE JC_PRODUCT  SET " +
            " SHOP_ID={0},PRODUCT_NAME={1},PRODUCT_DESCRIPTION={2},PRODUCT_WEIGHT={3},PRODUCT_WEIGHT_UNIT={4}," +
            " PRODUCT_LENGTH={5},PRODUCT_LENGTH_UNIT={6},PRODUCT_DEPTH={7},PRODUCT_DEPTH_UNIT={8}," +
            " PRODUCT_WIDTH={9},PRODUCT_WIDTH_UNIT={10},PRODUCT_HEIGHT={11},PRODUCT_HEIGHT_UNIT={12},PRODUCT_PRICE={13},PRODUCT_CURRENCY={14},PRODUCT_INVENTORY={15}," +
            " PRODUCT_ORIGIN={16},PICTURE_PATH= {17}, PRODUCT_MODIFICATION_DATE=TO_DATE(sysdate)" +
            " WHERE PRODUCT_ID = {18}";
        public static string SqlQuery_Update(int idShop, string name, string description, int? weight, string weightUnit, int? lenght, string lenghtUnit, int? depth, string depthUnit,
           int? width, string widthUnit, int? height, string heightUnit, float? prix, string currency, int? inventory, string origin,string picturePath, int id)
        {
            QueryHelper helper = new QueryHelper();
            return string.Format(SQLQUERY_PRODUCT_ORDER_PATTERN, idShop, helper.ConvertNullValues(name), helper.ConvertNullValues(description), helper.ConvertNullValues(weight), helper.ConvertNullValues(weightUnit),
                helper.ConvertNullValues(lenght), helper.ConvertNullValues(lenghtUnit), helper.ConvertNullValues(depth), helper.ConvertNullValues(depthUnit), helper.ConvertNullValues(width), helper.ConvertNullValues(widthUnit),
                helper.ConvertNullValues(height), helper.ConvertNullValues(heightUnit), helper.ConvertNullValues(prix), helper.ConvertNullValues(currency), helper.ConvertNullValues(inventory), helper.ConvertNullValues(origin), helper.ConvertNullValues(picturePath), id);
        }

        private const string SQLQUERY_SELECT_LAST_PRODUCT_PATTERN = "SELECT PRODUCT_ID," +
            " SHOP_ID,PRODUCT_NAME,PRODUCT_DESCRIPTION,PRODUCT_WEIGHT,PRODUCT_WEIGHT_UNIT," +
            " PRODUCT_LENGTH,PRODUCT_LENGTH_UNIT,PRODUCT_DEPTH,PRODUCT_DEPTH_UNIT," +
            " PRODUCT_WIDTH,PRODUCT_WIDTH_UNIT,PRODUCT_HEIGHT,PRODUCT_HEIGHT_UNIT,PRODUCT_PRICE,PRODUCT_CURRENCY,PRODUCT_INVENTORY,PRODUCT_CREATION_DATE,PRODUCT_MODIFICATION_DATE, PRODUCT_DELETED,PRODUCT_ORIGIN, PRODUCT_PICTURE_ID, PICTURE_PATH FROM JC_PRODUCT " +
            " WHERE PRODUCT_DELETED =0 ORDER BY PRODUCT_CREATION_DATE DESC";
        public static string SqlQuery_SelectLastProduct()
        {
            return SQLQUERY_SELECT_LAST_PRODUCT_PATTERN;
        }

        private const string SQLQUERY_INSERT_PRODUCT_PICTURE_PATH_PATTERN = "UPDATE JC_PRODUCT  SET " +
          "PICTURE_PATH={0}" +
          "WHERE PRODUCT_ID = {1}";
        public static string SqlQuery_InsertPicturePath(string picturePath, int id)
        {
            QueryHelper helper = new QueryHelper();
            return string.Format(SQLQUERY_INSERT_PRODUCT_PICTURE_PATH_PATTERN, helper.ConvertNullValues(picturePath), id);
        }
    }
}
