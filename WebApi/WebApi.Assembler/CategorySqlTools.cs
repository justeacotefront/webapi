﻿namespace WebApi.DataAccess
{
    public static class CategorySqlTools
    {
        /// <summary>
        /// Description de la table JC_CATEGORY.
        /// </summary>
        public static class CategoryTable
        {
            /// <summary>
            /// Nom de ma colonne Parent dans la table.
            /// </summary>
            public const string Parent = "CATEGORY_PARENT";

            /// <summary>
            /// Nom de ma colonne Id dans la table.
            /// </summary>
            public const string Id = "CATEGORY_ID";

            /// <summary>
            /// Nom de la colonne Description dans la table.
            /// </summary>
            public const string Description = "CATEGORY_DESCRIPTION";

            /// <summary>
            /// Nom de la colonne DateCreation dans la table.
            /// </summary>
            public const string DateCreation = "CATEGORY_CREATION_DATE";

            /// <summary>
            /// Nom de la colonne DateModification dans la table.
            /// </summary>
            public const string DateModification = "CATEGORY_MODIFICATION_DATE";
        }

        private const string SQLQUERY_INSERT_CATEGORY_PATTERN = "INSERT INTO JC_CATEGORY (CATEGORY_PARENT, CATEGORY_DESCRIPTION, CATEGORY_CREATION_DATE) VALUES ({0}, '{1}', TO_DATE(sysdate))";
        public static string SqlQuery_InsertCategory(int parent, string description)
        {
            return string.Format(SQLQUERY_INSERT_CATEGORY_PATTERN, parent, description.Replace("'", "''"));
        }

        private const string SQLQUERY_INSERT_SIMPLE_CATEGORY_PATTERN = "INSERT INTO JC_CATEGORY (CATEGORY_DESCRIPTION, CATEGORY_CREATION_DATE) VALUES ({0}, TO_DATE(sysdate))";
        public static string SqlQuery_InsertSimpleCategory(string description)
        {
            return string.Format(SQLQUERY_INSERT_SIMPLE_CATEGORY_PATTERN, description.Replace("'", "''"));
        }

        private const string SQLQUERY_SELECT_ALL_CATEGORY_PATTERN = "SELECT CATEGORY_PARENT, CATEGORY_ID, CATEGORY_DESCRIPTION, CATEGORY_CREATION_DATE,CATEGORY_MODIFICATION_DATE FROM JC_CATEGORY";
        public static string SqlQuery_SelectAll()
        {
            return SQLQUERY_SELECT_ALL_CATEGORY_PATTERN;
        }

        private const string SQLQUERY_SELECT_CATEGORY_BY_ID_PATTERN = "SELECT CATEGORY_PARENT, CATEGORY_ID, CATEGORY_DESCRIPTION, CATEGORY_CREATION_DATE,CATEGORY_MODIFICATION_DATE FROM JC_CATEGORY WHERE CATEGORY_ID = {0}";
        public static string SqlQuery_SelectById(int id)
        {
            return string.Format(SQLQUERY_SELECT_CATEGORY_BY_ID_PATTERN, id);
        }

        private const string SQLQUERY_DELETE_CATEGORY_BY_ID_PATTERN = "DELETE FROM JC_CATEGORY WHERE CATEGORY_ID = {0}";
        public static string SqlQuery_Delete(int id)
        {
            return string.Format(SQLQUERY_DELETE_CATEGORY_BY_ID_PATTERN, id);
        }

        private const string SQLQUERY_UPDATE_CATEGORY_PATTERN = "UPDATE JC_CATEGORY  SET CATEGORY_PARENT = {0}, CATEGORY_DESCRIPTION = '{1}', CATEGORY_MODIFICATION_DATE=TO_DATE(sysdate) WHERE CATEGORY_ID = {2}";
        public static string SqlQuery_Update(int id, int parent, string description)
        {
            return string.Format(SQLQUERY_UPDATE_CATEGORY_PATTERN, parent, description.Replace("'", "''"), id);
        }

        private const string SQLQUERY_UPDATE_SIMPLE_CATEGORY_PATTERN = "UPDATE JC_CATEGORY  SET CATEGORY_DESCRIPTION = '{0}', CATEGORY_MODIFICATION_DATE=TO_DATE(sysdate) WHERE CATEGORY_ID = {1}";
        public static string SqlQuery_UpdateSimple(int id, string description)
        {
            return string.Format(SQLQUERY_UPDATE_SIMPLE_CATEGORY_PATTERN,  description.Replace("'", "''"), id);
        }
    }
}
