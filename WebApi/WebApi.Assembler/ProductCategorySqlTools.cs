﻿namespace WebApi.Assembler
{
    public class ProductCategorySqlTools
    {
        /// <summary>
        /// Description de la table JC_PRODUCT_CATEGORY.
        /// </summary>
        public static class ProductCategoryTable
        {
            /// <summary>
            /// Nom de ma colonne CategoryId dans la table.
            /// </summary>
            public const string CategoryId = "CATEGORY_ID";

            /// <summary>
            /// Nom de ma colonne ProductId dans la table.
            /// </summary>
            public const string ProductId = "PRODUCT_ID";

        }

        private const string SQLQUERY_INSERT_PRODUCT_CATEGORY_PATTERN = "INSERT INTO JC_PRODUCT_CATEGORY (CATEGORY_ID, PRODUCT_ID) VALUES ({0},{1})";
        public static string SqlQuery_Insert(int CategoryId, int ProductId)
        {
            return string.Format(SQLQUERY_INSERT_PRODUCT_CATEGORY_PATTERN, CategoryId, ProductId);
        }

        private const string SQLQUERY_SELECT_ALL_PRODUCT_CATEGORY_PATTERN = "SELECT CATEGORY_ID, PRODUCT_ID FROM JC_PRODUCT_CATEGORY";
        public static string SqlQuery_SelectAll()
        {
            return SQLQUERY_SELECT_ALL_PRODUCT_CATEGORY_PATTERN;
        }

        private const string SQLQUERY_SELECT_PRODUCT_CATEGORY_BY_IDPRODUCT_PATTERN = "SELECT CATEGORY_ID, PRODUCT_ID FROM JC_PRODUCT_CATEGORY WHERE PRODUCT_ID = {0}";
        public static string SqlQuery_SelectByIdProduct(int ProductId)
        {
            return string.Format(SQLQUERY_SELECT_PRODUCT_CATEGORY_BY_IDPRODUCT_PATTERN, ProductId);
        }
        private const string SQLQUERY_SELECT_PRODUCT_CATEGORY_BY_IDCATEGORY_PATTERN = "SELECT CATEGORY_ID, PRODUCT_ID FROM JC_PRODUCT_CATEGORY WHERE CATEGORY_ID = {0}";
        public static string SqlQuery_SelectByIdCategory(int CategoryId)
        {
            return string.Format(SQLQUERY_SELECT_PRODUCT_CATEGORY_BY_IDCATEGORY_PATTERN, CategoryId);
        }

        private const string SQLQUERY_DELETE_PRODUCT_CATEGORY_BY_ID_PATTERN = "DELETE FROM JC_PRODUCT_CATEGORY WHERE CATEGORY_ID = {0} AND PRODUCT_ID={1}";
        public static string SqlQuery_Delete(int CategoryId, int ProductId)
        {
            return string.Format(SQLQUERY_DELETE_PRODUCT_CATEGORY_BY_ID_PATTERN, CategoryId, ProductId);
        }

    }
}
