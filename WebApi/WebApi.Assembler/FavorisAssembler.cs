﻿using System;
using Oracle.ManagedDataAccess.Client;
using WebApi.Transport;

namespace WebApi.Assembler
{
    public class FavorisAssembler : BaseAssembler<Favoris>
    {
        protected override void Populate(OracleDataReader source, Favoris target)
        {
            if (source.HasRows)
            {
                int colIndex = source.GetOrdinal(FavorisSqlTools.FavorisTable.ShopId);
                if (!source.IsDBNull(colIndex))
                    target.ShopId = Convert.ToInt32(source[FavorisSqlTools.FavorisTable.ShopId]);
                colIndex = source.GetOrdinal(FavorisSqlTools.FavorisTable.CustomerId);
                if (!source.IsDBNull(colIndex))
                    target.CustomerId = Convert.ToInt32(source[FavorisSqlTools.FavorisTable.CustomerId]);
                colIndex = source.GetOrdinal(FavorisSqlTools.FavorisTable.Creation);
                if (!source.IsDBNull(colIndex))
                    target.Creation = Convert.ToDateTime(source[FavorisSqlTools.FavorisTable.Creation]);

            }
        }
    }
}
