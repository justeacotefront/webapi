﻿using Oracle.ManagedDataAccess.Client;
using System;
using WebApi.DataAccess;
using WebApi.Transport;

namespace WebApi.Assembler
{
    public class CategoryAssembler : BaseAssembler<Category>
    {
        protected override void Populate(OracleDataReader source, Category target)
        {
            if (source.HasRows)
            {

                int colIndex = source.GetOrdinal(CategorySqlTools.CategoryTable.Parent);
                if (!source.IsDBNull(colIndex))
                    target.Parent = Convert.ToInt32(source[CategorySqlTools.CategoryTable.Parent]);
                colIndex = source.GetOrdinal(CategorySqlTools.CategoryTable.Id);
                if (!source.IsDBNull(colIndex))
                    target.Identifiant = Convert.ToInt32(source[CategorySqlTools.CategoryTable.Id]);
                colIndex = source.GetOrdinal(CategorySqlTools.CategoryTable.Description);
                if (!source.IsDBNull(colIndex))
                    target.Description = source[CategorySqlTools.CategoryTable.Description].ToString();
                colIndex = source.GetOrdinal(CategorySqlTools.CategoryTable.DateCreation);
                if (!source.IsDBNull(colIndex))
                    target.Creation = Convert.ToDateTime(source[CategorySqlTools.CategoryTable.DateCreation]);
                colIndex = source.GetOrdinal(CategorySqlTools.CategoryTable.DateModification);
                if (!source.IsDBNull(colIndex))
                    target.Modification = Convert.ToDateTime(source[CategorySqlTools.CategoryTable.DateModification]);
            }

        }
    }
}
