﻿using Oracle.ManagedDataAccess.Types;
using WebApi.Assembler.Helper;

namespace WebApi.Assembler
{
    public class PicturesSqlTools
    {
        /// <summary>
        /// Description de la table JC_PICTURES.
        /// </summary>
        public static class PicturesTable
        {
            /// <summary>
            /// Nom de ma colonne PICTURE_ID dans la table.
            /// </summary>
            public const string PictureId = "PICTURE_ID";

            /// <summary>
            /// Nom de ma colonne PICTURE_BLOB dans la table.
            /// </summary>
            public const string PictureBlob = "PICTURE_BLOB";

        }

        private const string SQLQUERY_INSERT_PICTURE_PATTERN = " INSERT INTO JC_PICTURES (PICTURE_BLOB) VALUES (" + " :BlobParameter )";
        public static string SqlQuery_Insert()
        {
            return SQLQUERY_INSERT_PICTURE_PATTERN;
        }

        private const string SQLQUERY_INSERT_PICTURE_RETURN_ID_PATTERN = " BEGIN INSERT INTO JC_PICTURES (PICTURE_BLOB) VALUES ("+ " :BlobParameter )RETURNING PICTURE_ID INTO :id;END;";
        public static string SqlQuery_InsertAndReturnPK()
        {
            return string.Format(SQLQUERY_INSERT_PICTURE_RETURN_ID_PATTERN);
        }
        private const string SQLQUERY_SELECT_ALL_PICTURES_PATTERN = "SELECT PICTURE_ID, PICTURE_BLOB FROM JC_PICTURES";
        public static string SqlQuery_SelectAll()
        {
            return SQLQUERY_SELECT_ALL_PICTURES_PATTERN;
        }

        private const string SQLQUERY_SELECT_PICTURES_BY_ID_PATTERN = "SELECT PICTURE_ID, PICTURE_BLOB FROM JC_PICTURES WHERE PICTURE_ID = {0}";
        public static string SqlQuery_SelectById(int id)
        {
            return string.Format(SQLQUERY_SELECT_PICTURES_BY_ID_PATTERN, id);
        }

        private const string SQLQUERY_DELETE_PICTURES_BY_ID_PATTERN = "DELETE FROM JC_PICTURES WHERE PICTURE_ID = {0}";
        public static string SqlQuery_Delete(int id)
        {
            return string.Format(SQLQUERY_DELETE_PICTURES_BY_ID_PATTERN, id);
        }

        private const string SQLQUERY_UPDATE_PICTURES_PATTERN = "UPDATE JC_PICTURES  SET PICTURE_BLOB = :BlobParameter WHERE PICTURE_ID = {0}";
        public static string SqlQuery_Update(int id)
        {
            return string.Format(SQLQUERY_UPDATE_PICTURES_PATTERN, id);
        }
    }
}
