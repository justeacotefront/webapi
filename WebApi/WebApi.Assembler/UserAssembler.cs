﻿using System;
using Oracle.ManagedDataAccess.Client;
using WebApi.Transport;

namespace WebApi.Assembler
{
    public class UserAssembler : BaseAssembler<User>
    {
        protected override void Populate(OracleDataReader source, User target)
        {
            if (source.HasRows)
            {
                int colIndex = source.GetOrdinal(UserSqlTools.UserTable.Id);
                if (!source.IsDBNull(colIndex))
                    target.Identifiant = Convert.ToInt32(source[UserSqlTools.UserTable.Id]);
                colIndex = source.GetOrdinal(UserSqlTools.UserTable.Login);
                if (!source.IsDBNull(colIndex))
                    target.Login = source[UserSqlTools.UserTable.Login].ToString();
                colIndex = source.GetOrdinal(UserSqlTools.UserTable.NumberLogin);
                if (!source.IsDBNull(colIndex))
                    target.NombreLogin = Convert.ToInt32(source[UserSqlTools.UserTable.NumberLogin]);
                colIndex = source.GetOrdinal(UserSqlTools.UserTable.MotDePasse);
                if (!source.IsDBNull(colIndex))
                    target.MDP = source[UserSqlTools.UserTable.MotDePasse].ToString();
                colIndex = source.GetOrdinal(UserSqlTools.UserTable.LastLogin);
                if (!source.IsDBNull(colIndex))
                    target.LastLogin = Convert.ToDateTime(source[UserSqlTools.UserTable.LastLogin]);
                colIndex = source.GetOrdinal(UserSqlTools.UserTable.DateCreation);
                if (!source.IsDBNull(colIndex))
                    target.DateCreation = Convert.ToDateTime(source[UserSqlTools.UserTable.DateCreation]);
                colIndex = source.GetOrdinal(UserSqlTools.UserTable.DateModification);
                if (!source.IsDBNull(colIndex))
                    target.DateModification = Convert.ToDateTime(source[UserSqlTools.UserTable.DateModification]);

            }
        }
    }
}
