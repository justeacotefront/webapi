﻿using Oracle.ManagedDataAccess.Client;
using System;
using WebApi.Transport;

namespace WebApi.Assembler
{
    public class ProductAssembler : BaseAssembler<Product>
    {
        protected override void Populate(OracleDataReader source, Product target)
        {
            if (source.HasRows)
            {
                int colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.Id);
                if (!source.IsDBNull(colIndex))
                    target.Identifiant = Convert.ToInt32(source[ProductSqlTools.ProductTable.Id]);
                colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.IdShop);
                if (!source.IsDBNull(colIndex))
                    target.ShopId = Convert.ToInt32(source[ProductSqlTools.ProductTable.IdShop]);
                colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.Nom);
                if (!source.IsDBNull(colIndex))
                    target.Name = source[ProductSqlTools.ProductTable.Nom].ToString();
                colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.Description);
                if (!source.IsDBNull(colIndex))
                    target.Description = source[ProductSqlTools.ProductTable.Description].ToString();
                
                colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.Weight);
                if (!source.IsDBNull(colIndex))
                    target.Weight = Convert.ToInt32(source[ProductSqlTools.ProductTable.Weight]);
                colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.WeightUnit);
                if (!source.IsDBNull(colIndex))
                    target.WeightUnit = source[ProductSqlTools.ProductTable.WeightUnit].ToString();
                colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.Lenght);
                if (!source.IsDBNull(colIndex))
                    target.Lenght = Convert.ToInt32(source[ProductSqlTools.ProductTable.Lenght]);
                colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.LenghtUnit);
                if (!source.IsDBNull(colIndex))
                    target.LenghtUnit = source[ProductSqlTools.ProductTable.LenghtUnit].ToString();
                colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.Depth);
                if (!source.IsDBNull(colIndex))
                    target.Depth = Convert.ToInt32(source[ProductSqlTools.ProductTable.Depth]);
                colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.DepthUnit);
                if (!source.IsDBNull(colIndex))
                    target.DepthUnit = source[ProductSqlTools.ProductTable.DepthUnit].ToString();
                colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.Width);
                if (!source.IsDBNull(colIndex))
                    target.Width = Convert.ToInt32(source[ProductSqlTools.ProductTable.Width]);
                colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.WidthUnit);
                if (!source.IsDBNull(colIndex))
                    target.WidthUnit = source[ProductSqlTools.ProductTable.WidthUnit].ToString();
                colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.Height);
                if (!source.IsDBNull(colIndex))
                    target.Height = Convert.ToInt32(source[ProductSqlTools.ProductTable.Height]);
                colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.HeightUnit);
                if (!source.IsDBNull(colIndex))
                    target.HeightUnit = source[ProductSqlTools.ProductTable.HeightUnit].ToString();
                colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.Prix);
                if (!source.IsDBNull(colIndex))
                    target.Price = Convert.ToInt32(source[ProductSqlTools.ProductTable.Prix]);
                colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.Currency);
                if (!source.IsDBNull(colIndex))
                    target.Currency = source[ProductSqlTools.ProductTable.Currency].ToString();
                colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.Inventory);
                if (!source.IsDBNull(colIndex))
                    target.Inventory = Convert.ToInt32(source[ProductSqlTools.ProductTable.Inventory]);
                colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.DateCreation);
                if (!source.IsDBNull(colIndex))
                    target.Creation = Convert.ToDateTime(source[ProductSqlTools.ProductTable.DateCreation]);
                colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.DateModification);
                if (!source.IsDBNull(colIndex))
                    target.Modification = Convert.ToDateTime(source[ProductSqlTools.ProductTable.DateModification]);
                colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.Supprimer);
                if (!source.IsDBNull(colIndex))
                    target.Supprimer = Convert.ToInt32(source[ProductSqlTools.ProductTable.Supprimer]) == 1 ? true : false;
                colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.Origin);
                if (!source.IsDBNull(colIndex))
                    target.Origin = source[ProductSqlTools.ProductTable.Origin].ToString();
                colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.IdPicture);
                if (!source.IsDBNull(colIndex))
                    target.IdImage = Convert.ToInt32(source[ProductSqlTools.ProductTable.IdPicture]);
                colIndex = source.GetOrdinal(ProductSqlTools.ProductTable.PicturePath);
                if (!source.IsDBNull(colIndex))
                    target.PicturePath = source[ProductSqlTools.ProductTable.PicturePath].ToString();
            }
        }
    }
}
