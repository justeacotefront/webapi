﻿using System;
using WebApi.Assembler.Helper;

namespace WebApi.Assembler
{
    public static class ShopSqlTools
    {
        /// <summary>
        /// Description de la table JC_SHOP.
        /// </summary>
        public static class ShopTable
        {
            /// <summary>
            /// Nom de ma colonne Id dans la table.
            /// </summary>
            public const string Id = "SHOP_ID";

            /// <summary>
            /// Nom de la colonne Nom dans la table.
            /// </summary>
            public const string Nom = "SHOP_NAME";

            /// <summary>
            /// Nom de la colonne Description dans la table.
            /// </summary>
            public const string Description = "SHOP_DESCRIPTION";

            /// <summary>
            /// Nom de la colonne Type dans la table.
            /// </summary>
            public const string Type = "SHOP_TYPE";

            /// <summary>
            /// Nom de la colonne Rue dans la table.
            /// </summary>
            public const string Rue = "SHOP_STREET";

            /// <summary>
            /// Nom de la colonne CodePostal dans la table.
            /// </summary>
            public const string CodePostal = "SHOP_POSTAL_CODE";

            /// <summary>
            /// Nom de la colonne Ville dans la table.
            /// </summary>
            public const string Ville = "SHOP_CITY";

            /// <summary>
            /// Nom de la colonne CreationShop dans la table.
            /// </summary>
            public const string CreationShop = "SHOP_CREATION";

            /// <summary>
            /// Nom de la colonne Createur dans la table.
            /// </summary>
            public const string Createur = "SHOP_CREATOR";

            /// <summary>
            /// Nom de la colonne LegalStatut dans la table.
            /// </summary>
            public const string LegalStatut = "SHOP_LEGAL_STATUT";

            /// <summary>
            /// Nom de la colonne NbEmployees dans la table.
            /// </summary>
            public const string NbEmployees = "SHOP_NB_EMPLOYEES";

            /// <summary>
            /// Nom de la colonne TurnOverRange1 dans la table.
            /// </summary>
            public const string TurnOverRange1 = "SHOP_TURNOVER_RANGE_1";

            /// <summary>
            /// Nom de la colonne TurnOverRange2 dans la table.
            /// </summary>
            public const string TurnOverRange2 = "SHOP_TURNOVER_RANGE_2";

            /// <summary>
            /// Nom de la colonne DateCreation dans la table.
            /// </summary>
            public const string DateCreation = "SHOP_CREATION_DATE";

            /// <summary>
            /// Nom de la colonne DateModification dans la table.
            /// </summary>
            public const string DateModification = "SHOP_MODIFICATION_DATE";

            /// <summary>
            /// Nom de ma colonne IdPicture dans la table.
            /// </summary>
            public const string IdPicture = "SHOP_PICTURE_ID";

            /// <summary>
            /// Nom de la colonne PicturePath dans la table
            /// </summary>
            public const string PicturePath = "PICUTRE_PATH"; 
        }

        private const string SQLQUERY_INSERT_SHOP_PATTERN = "INSERT INTO JC_SHOP " +
            "(SHOP_NAME,SHOP_DESCRIPTION,SHOP_TYPE,SHOP_STREET," +
            " SHOP_POSTAL_CODE,SHOP_CITY,SHOP_CREATION,SHOP_CREATOR," +
            "SHOP_LEGAL_STATUT,SHOP_NB_EMPLOYEES,SHOP_TURNOVER_RANGE_1,SHOP_TURNOVER_RANGE_2, PICUTRE_PATH," +
            " SHOP_CREATION_DATE) " +
            "VALUES " +
            "({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}, TO_DATE(sysdate))";
        public static string SqlQuery_Insert( string name, string description, string type, string rue, int? codePostal, string ville, DateTime? creationShop, string createur,
           string legalStatus, int? nbEmployees, int? turnOverRange1, int? turnOverRange2, string picturePath)
        {
            QueryHelper helper = new QueryHelper();
            return string.Format(SQLQUERY_INSERT_SHOP_PATTERN, helper.ConvertNullValues(name), helper.ConvertNullValues(description), helper.ConvertNullValues(type), helper.ConvertNullValues(rue), helper.ConvertNullValues(codePostal), helper.ConvertNullValues(ville),
                helper.ConvertDateNullValues(creationShop), helper.ConvertNullValues(createur), helper.ConvertNullValues(legalStatus), helper.ConvertNullValues(nbEmployees), helper.ConvertNullValues(turnOverRange1), helper.ConvertNullValues(turnOverRange2), helper.ConvertNullValues(picturePath));
        }

        private const string SQLQUERY_SELECT_ALL_SHOP_PATTERN = "SELECT SHOP_ID,SHOP_NAME,SHOP_DESCRIPTION,SHOP_TYPE,SHOP_STREET," +
            "SHOP_POSTAL_CODE,SHOP_CITY,SHOP_CREATION,SHOP_CREATOR," +
            "SHOP_LEGAL_STATUT,SHOP_NB_EMPLOYEES,SHOP_TURNOVER_RANGE_1,SHOP_TURNOVER_RANGE_2," +
            "SHOP_CREATION_DATE,SHOP_MODIFICATION_DATE, SHOP_PICTURE_ID, PICUTRE_PATH " +
            "FROM JC_SHOP";
        public static string SqlQuery_SelectAll()
        {
            return SQLQUERY_SELECT_ALL_SHOP_PATTERN;
        }

        private const string SQLQUERY_SELECT_SHOP_BY_ID_PATTERN = "SELECT SHOP_ID,SHOP_NAME,SHOP_DESCRIPTION,SHOP_TYPE,SHOP_STREET," +
            "SHOP_POSTAL_CODE,SHOP_CITY,SHOP_CREATION,SHOP_CREATOR," +
            "SHOP_LEGAL_STATUT,SHOP_NB_EMPLOYEES,SHOP_TURNOVER_RANGE_1,SHOP_TURNOVER_RANGE_2," +
            "SHOP_CREATION_DATE,SHOP_MODIFICATION_DATE, SHOP_PICTURE_ID, PICUTRE_PATH " +
            "FROM JC_SHOP WHERE SHOP_ID = {0}";
        public static string SqlQuery_SelectById(int id)
        {
            return string.Format(SQLQUERY_SELECT_SHOP_BY_ID_PATTERN, id);
        }

        private const string SQLQUERY_DELETE_SHOP_BY_ID_PATTERN = "DELETE FROM JC_SHOP WHERE SHOP_ID = {0}";
        public static string SqlQuery_Delete(int id)
        {
            return string.Format(SQLQUERY_DELETE_SHOP_BY_ID_PATTERN, id);
        }

        private const string SQLQUERY_SHOP_ORDER_PATTERN = "UPDATE JC_SHOP  SET " +
            "SHOP_NAME={0},SHOP_DESCRIPTION={1},SHOP_TYPE={2},SHOP_STREET={3}," +
            "SHOP_POSTAL_CODE={4},SHOP_CITY={5},SHOP_CREATION={6},SHOP_CREATOR={7}," +
            "SHOP_LEGAL_STATUT={8},SHOP_NB_EMPLOYEES={9},SHOP_TURNOVER_RANGE_1={10},SHOP_TURNOVER_RANGE_2={11},PICUTRE_PATH={12}," + 
            "SHOP_MODIFICATION_DATE=TO_DATE(sysdate)" +
            "WHERE SHOP_ID = {13}";
        public static string SqlQuery_Update(string name, string description, string type, string rue, int? codePostal, string ville, DateTime? creationShop, string createur,
           string legalStatus, int? nbEmployees, int? turnOverRange1, int? turnOverRange2, string picturePath,int id)
        {
            QueryHelper helper = new QueryHelper();
            return string.Format(SQLQUERY_SHOP_ORDER_PATTERN, helper.ConvertNullValues(name), helper.ConvertNullValues(description), helper.ConvertNullValues(type), helper.ConvertNullValues(rue), helper.ConvertNullValues(codePostal), helper.ConvertNullValues(ville),
                helper.ConvertDateNullValues(creationShop), helper.ConvertNullValues(createur), helper.ConvertNullValues(legalStatus), helper.ConvertNullValues(nbEmployees), helper.ConvertNullValues(turnOverRange1), helper.ConvertNullValues(turnOverRange2), helper.ConvertNullValues(picturePath), id);
        }

        private const string SQLQUERY_INSERT_SHOP_PICTURE_PATH_PATTERN = "UPDATE JC_SHOP  SET " +
            "PICUTRE_PATH={0}" +
            "WHERE SHOP_ID = {1}";
        public static string SqlQuery_InsertPicturePath( string picturePath, int id)
        {
            QueryHelper helper = new QueryHelper();
            return string.Format(SQLQUERY_INSERT_SHOP_PICTURE_PATH_PATTERN, helper.ConvertNullValues(picturePath), id);
        }

    }
}
