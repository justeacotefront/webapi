﻿using System;
using Oracle.ManagedDataAccess.Client;
using WebApi.Transport;

namespace WebApi.Assembler
{
    public class ShopAssembler : BaseAssembler<Shop>
    {
        protected override void Populate(OracleDataReader source, Shop target)
        {
            if (source.HasRows)
            {
                int colIndex = source.GetOrdinal(ShopSqlTools.ShopTable.Id);
                if (!source.IsDBNull(colIndex))
                    target.Identifiant = Convert.ToInt32(source[ShopSqlTools.ShopTable.Id]);
                colIndex = source.GetOrdinal(ShopSqlTools.ShopTable.Nom);
                if (!source.IsDBNull(colIndex))
                    target.Nom = source[ShopSqlTools.ShopTable.Nom].ToString();
                colIndex = source.GetOrdinal(ShopSqlTools.ShopTable.Description);
                if (!source.IsDBNull(colIndex))
                    target.Description = source[ShopSqlTools.ShopTable.Description].ToString();
                colIndex = source.GetOrdinal(ShopSqlTools.ShopTable.Type);
                if (!source.IsDBNull(colIndex))
                    target.Type = source[ShopSqlTools.ShopTable.Type].ToString();
                colIndex = source.GetOrdinal(ShopSqlTools.ShopTable.Rue);
                if (!source.IsDBNull(colIndex))
                    target.Rue = source[ShopSqlTools.ShopTable.Rue].ToString();
                colIndex = source.GetOrdinal(ShopSqlTools.ShopTable.CodePostal);
                if (!source.IsDBNull(colIndex))
                    target.CodePostale = Convert.ToInt32(source[ShopSqlTools.ShopTable.CodePostal]);
                colIndex = source.GetOrdinal(ShopSqlTools.ShopTable.Ville);
                if (!source.IsDBNull(colIndex))
                    target.Ville = source[ShopSqlTools.ShopTable.Ville].ToString();
                colIndex = source.GetOrdinal(ShopSqlTools.ShopTable.CreationShop);
                if (!source.IsDBNull(colIndex))
                    target.CreationShop = Convert.ToDateTime(source[ShopSqlTools.ShopTable.CreationShop]);
                colIndex = source.GetOrdinal(ShopSqlTools.ShopTable.Createur);
                if (!source.IsDBNull(colIndex))
                    target.Createur = source[ShopSqlTools.ShopTable.Createur].ToString();
                colIndex = source.GetOrdinal(ShopSqlTools.ShopTable.LegalStatut);
                if (!source.IsDBNull(colIndex))
                    target.StatutLegal = source[ShopSqlTools.ShopTable.LegalStatut].ToString();
                colIndex = source.GetOrdinal(ShopSqlTools.ShopTable.NbEmployees);
                if (!source.IsDBNull(colIndex))
                    target.NombreEmploye = Convert.ToInt32(source[ShopSqlTools.ShopTable.NbEmployees]);
                colIndex = source.GetOrdinal(ShopSqlTools.ShopTable.TurnOverRange1);
                if (!source.IsDBNull(colIndex))
                    target.TurnOverRange1 = Convert.ToInt32(source[ShopSqlTools.ShopTable.TurnOverRange1]);
                colIndex = source.GetOrdinal(ShopSqlTools.ShopTable.TurnOverRange2);
                if (!source.IsDBNull(colIndex))
                    target.TurnOverRange2 = Convert.ToInt32(source[ShopSqlTools.ShopTable.TurnOverRange2]);
                colIndex = source.GetOrdinal(ShopSqlTools.ShopTable.DateCreation);
                if (!source.IsDBNull(colIndex))
                    target.CreationDate = Convert.ToDateTime(source[ShopSqlTools.ShopTable.DateCreation]);
                colIndex = source.GetOrdinal(ShopSqlTools.ShopTable.DateModification);
                if (!source.IsDBNull(colIndex))
                    target.Modification = Convert.ToDateTime(source[ShopSqlTools.ShopTable.DateModification]);
                colIndex = source.GetOrdinal(ShopSqlTools.ShopTable.IdPicture);
                if (!source.IsDBNull(colIndex))
                    target.IdImage = Convert.ToInt32(source[ShopSqlTools.ShopTable.IdPicture]);
                colIndex = source.GetOrdinal(ShopSqlTools.ShopTable.PicturePath);
                if(!source.IsDBNull(colIndex))
                    target.PicturePath = source[ShopSqlTools.ShopTable.PicturePath].ToString();
            }
        }
    }
}
