﻿using Oracle.ManagedDataAccess.Client;
using System;
using WebApi.Transport;

namespace WebApi.Assembler
{
    public class OrderLineAssembler : BaseAssembler<OrderLine>
    {
        protected override void Populate(OracleDataReader source, OrderLine target)
        {
            if (source.HasRows)
            {
                int colIndex = source.GetOrdinal(OrderLineSqlTools.OrderLineTable.Id);
                if (!source.IsDBNull(colIndex))
                    target.IdOrderLine = Convert.ToInt32(source[OrderLineSqlTools.OrderLineTable.Id]);
                colIndex = source.GetOrdinal(OrderLineSqlTools.OrderLineTable.IdOrder);
                if (!source.IsDBNull(colIndex))
                    target.IdOrder = Convert.ToInt32(source[OrderLineSqlTools.OrderLineTable.IdOrder]);
                colIndex = source.GetOrdinal(OrderLineSqlTools.OrderLineTable.IdProduct);
                if (!source.IsDBNull(colIndex))
                    target.IdProduct = Convert.ToInt32(source[OrderLineSqlTools.OrderLineTable.IdProduct]);
                colIndex = source.GetOrdinal(OrderLineSqlTools.OrderLineTable.PrixHT);
                if (!source.IsDBNull(colIndex))
                    target.PrixHT = Convert.ToInt32(source[OrderLineSqlTools.OrderLineTable.PrixHT]);
                colIndex = source.GetOrdinal(OrderLineSqlTools.OrderLineTable.Quantity);
                if (!source.IsDBNull(colIndex))
                    target.Quantite = Convert.ToInt32(source[OrderLineSqlTools.OrderLineTable.Quantity]);
                colIndex = source.GetOrdinal(OrderLineSqlTools.OrderLineTable.VAT);
                if (!source.IsDBNull(colIndex))
                    target.VAT = Convert.ToInt32(source[OrderLineSqlTools.OrderLineTable.VAT]);
                colIndex = source.GetOrdinal(OrderLineSqlTools.OrderLineTable.DateCreation);
                if (!source.IsDBNull(colIndex))
                    target.Creation = Convert.ToDateTime(source[OrderLineSqlTools.OrderLineTable.DateCreation]);
                colIndex = source.GetOrdinal(OrderLineSqlTools.OrderLineTable.DateModification);
                if (!source.IsDBNull(colIndex))
                    target.Modification = Convert.ToDateTime(source[OrderLineSqlTools.OrderLineTable.DateModification]);
            }
        }
    }
}
