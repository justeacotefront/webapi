﻿using WebApi.Assembler.Helper;

namespace WebApi.Assembler
{
    public static class SellerSqlTools
    {
        /// <summary>
        /// Description de la table JC_SELLER.
        /// </summary>
        public static class SellerTable
        {
            /// <summary>
            /// Nom de ma colonne IdSeller dans la table.
            /// </summary>
            public const string IdSeller = "SELLER_ID";

            /// <summary>
            /// Nom de ma colonne IdShop dans la table.
            /// </summary>
            public const string IdShop = "SHOP_ID";

            /// <summary>
            /// Nom de la colonne FirstName dans la table.
            /// </summary>
            public const string FirstName = "SELLER_FIRSTNAME";

            /// <summary>
            /// Nom de la colonne LastName dans la table.
            /// </summary>
            public const string LastName = "SELLER_LASTNAME";

            /// <summary>
            /// Nom de la colonne Phone dans la table.
            /// </summary>
            public const string Phone = "SELLER_PHONE";

            /// <summary>
            /// Nom de la colonne Email dans la table.
            /// </summary>
            public const string Email = "SELLER_EMAIL";

            /// <summary>
            /// Nom de la colonne Function dans la table.
            /// </summary>
            public const string Function = "SELLER_FUNCTION";

            /// <summary>
            /// Nom de la colonne Ville dans la table.
            /// </summary>
            public const string DateCreation = "SELLER_CREATION_DATE";

            /// <summary>
            /// Nom de la colonne CreatedBy dans la table.
            /// </summary>
            public const string CreatedBy = "SELLER_CREATED_BY";

            /// <summary>
            /// Nom de la colonne DateModification dans la table.
            /// </summary>
            public const string DateModification = "SELLER_MODIFICATION_DATE";

            /// <summary>
            /// Nom de la colonne UserId dans la table.
            /// </summary>
            public const string UserId = "USER_ID";

            /// <summary>
            /// Nom de la colonne SELLER_PICTURE_ID dans la table.
            /// </summary>
            public const string IdPicture = "SELLER_PICTURE_ID";

            /// <summary>
            /// Nom de la colonne PicturePath dans la table
            /// </summary>
            public const string PicturePath = "PICUTRE_PATH";
        }

        private const string SQLQUERY_INSERT_SELLER_PATTERN = "INSERT INTO JC_SELLER " +
            "(SHOP_ID,SELLER_FIRSTNAME,SELLER_LASTNAME,SELLER_PHONE,SELLER_EMAIL," +
            " SELLER_FUNCTION,SELLER_CREATED_BY,USER_ID,SELLER_CREATION_DATE, SELLER_ID, PICUTRE_PATH) " +
            " VALUES " +
            " ({0},{1},{2},{3},{4},{5},{6},{7}, TO_DATE(sysdate), {8}, {9})";
        public static string SqlQuery_Insert(int idShop, string firstname, string lastname, string phone, string email, string function,
            int? createdBy, int? userId, int identifiant, string picturePath)
        {
            QueryHelper helper = new QueryHelper();
            return string.Format(SQLQUERY_INSERT_SELLER_PATTERN, helper.ConvertNullValues(idShop), helper.ConvertNullValues(firstname), helper.ConvertNullValues(lastname), helper.ConvertNullValues(phone), helper.ConvertNullValues(email),
                helper.ConvertNullValues(function), helper.ConvertNullValues(createdBy), helper.ConvertNullValues(userId), helper.ConvertNullValues(identifiant), helper.ConvertNullValues(picturePath));
        }

        private const string SQLQUERY_SELECT_ALL_SELLER_PATTERN = "SELECT SELLER_ID, SHOP_ID,SELLER_FIRSTNAME,SELLER_LASTNAME,SELLER_PHONE,SELLER_EMAIL," +
            " SELLER_FUNCTION,SELLER_CREATED_BY,SELLER_CREATION_DATE,SELLER_MODIFICATION_DATE,USER_ID , SELLER_PICTURE_ID, PICUTRE_PATH " +
            " FROM JC_SELLER";
        public static string SqlQuery_SelectAll()
        {
            return SQLQUERY_SELECT_ALL_SELLER_PATTERN;
        }

        private const string SQLQUERY_SELECT_SELLER_BY_ID_PATTERN = "SELECT SELLER_ID, SHOP_ID,SELLER_FIRSTNAME,SELLER_LASTNAME,SELLER_PHONE,SELLER_EMAIL," +
            " SELLER_FUNCTION,SELLER_CREATED_BY,SELLER_CREATION_DATE,SELLER_MODIFICATION_DATE,USER_ID, SELLER_PICTURE_ID, PICUTRE_PATH  " +
            " FROM JC_SELLER WHERE SELLER_ID = {0}";
        public static string SqlQuery_SelectById(int id)
        {
            return string.Format(SQLQUERY_SELECT_SELLER_BY_ID_PATTERN, id);
        }

        private const string SQLQUERY_SELECT_SELLER_BY_IDSHOP_PATTERN = "SELECT SELLER_ID, SHOP_ID,SELLER_FIRSTNAME,SELLER_LASTNAME,SELLER_PHONE,SELLER_EMAIL," +
            " SELLER_FUNCTION,SELLER_CREATED_BY,SELLER_CREATION_DATE,SELLER_MODIFICATION_DATE,USER_ID, SELLER_PICTURE_ID, PICUTRE_PATH  " +
            " FROM JC_SELLER WHERE SHOP_ID = {0}";
        public static string SqlQuery_SelectByIdShop(int id)
        {
            return string.Format(SQLQUERY_SELECT_SELLER_BY_IDSHOP_PATTERN, id);
        }

        private const string SQLQUERY_DELETE_SELLER_BY_ID_PATTERN = "DELETE FROM JC_SELLER WHERE SELLER_ID = {0}";
        public static string SqlQuery_Delete(int id)
        {
            return string.Format(SQLQUERY_DELETE_SELLER_BY_ID_PATTERN, id);
        }

        private const string SQLQUERY_SELLER_ORDER_PATTERN = "UPDATE JC_SELLER  SET " +
            " SELLER_FIRSTNAME={0},SELLER_LASTNAME={1},SELLER_PHONE={2},SELLER_EMAIL={3}," +
            " SELLER_FUNCTION={4},SELLER_CREATED_BY={5},USER_ID={6},PICUTRE_PATH={7}," +
            " SELLER_MODIFICATION_DATE=TO_DATE(sysdate)" +
            " WHERE SELLER_ID = {8}";
        public static string SqlQuery_Update(string firstname, string lastname, string phone, string email, string function,
            int? createdBy, int? userId,string picturePath, int id)
        {
            QueryHelper helper = new QueryHelper();
            return string.Format(SQLQUERY_SELLER_ORDER_PATTERN, helper.ConvertNullValues(firstname), helper.ConvertNullValues(lastname), helper.ConvertNullValues(phone), helper.ConvertNullValues(email),
                helper.ConvertNullValues(function), helper.ConvertNullValues(createdBy), helper.ConvertNullValues(userId), helper.ConvertNullValues(picturePath), id);
        }

        private const string SQLQUERY_UPDATE_CUSTOMER_MAIL_PATTERN = "UPDATE JC_SELLER  SET " +
        " SELLER_EMAIL={0}," +
        " SELLER_MODIFICATION_DATE=TO_DATE(sysdate) " +
        " WHERE SELLER_ID = {1}";
        public static string SqlQuery_Update_Email(string email, int id)
        {
            QueryHelper helper = new QueryHelper();
            return string.Format(SQLQUERY_UPDATE_CUSTOMER_MAIL_PATTERN, helper.ConvertNullValues(email), id);
        }
        private const string SQLQUERY_INSERT_SELLER_PICTURE_PATH_PATTERN = "UPDATE JC_SELLER  SET " +
            "PICUTRE_PATH={0}" +
            "WHERE SELLER_ID = {1}";
        public static string SqlQuery_InsertPicturePath(string picturePath, int id)
        {
            QueryHelper helper = new QueryHelper();
            return string.Format(SQLQUERY_INSERT_SELLER_PICTURE_PATH_PATTERN, helper.ConvertNullValues(picturePath), id);
        }
    }
}
