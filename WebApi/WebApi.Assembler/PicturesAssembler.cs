﻿using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using WebApi.Transport;

namespace WebApi.Assembler
{
    public class PicturesAssembler : BaseAssembler<Picture>
    {
        protected override void Populate(OracleDataReader source, Picture target)
        {
            if (source.HasRows)
            {
                int colIndex = source.GetOrdinal(PicturesSqlTools.PicturesTable.PictureId);
                if (!source.IsDBNull(colIndex))
                    target.Identifiant = Convert.ToInt32(source[PicturesSqlTools.PicturesTable.PictureId]);
                colIndex = source.GetOrdinal(PicturesSqlTools.PicturesTable.PictureBlob);
                if (!source.IsDBNull(colIndex))
                {
                    OracleBlob Blob = source.GetOracleBlob(colIndex);
                    target.Blob = (source.GetOracleBlob(colIndex)).Value;
                }
            }
        }
    }
}

