﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApi.Assembler.Helper;

namespace WebApi.Assembler
{
    public static class UserSqlTools
    {
        /// <summary>
        /// Description de la table JC_USER.
        /// </summary>
        public static class UserTable
        {
            /// <summary>
            /// Nom de ma colonne Id dans la table.
            /// </summary>
            public const string Id = "USER_ID";

            /// <summary>
            /// Nom de ma colonne Login dans la table.
            /// </summary>
            public const string Login = "USER_LOGIN";

            /// <summary>
            /// Nom de la colonne MotDePasse dans la table.
            /// </summary>
            public const string MotDePasse = "USER_PWD";

            /// <summary>
            /// Nom de la colonne NumberLogin dans la table.
            /// </summary>
            public const string NumberLogin = "USER_NUMBER_LOGIN";

            /// <summary>
            /// Nom de la colonne LastLogin dans la table.
            /// </summary>
            public const string LastLogin = "USER_LAST_LOGIN";

            /// <summary>
            /// Nom de la colonne DateCreation dans la table.
            /// </summary>
            public const string DateCreation = "USER_CREATION_DATE";

            /// <summary>
            /// Nom de la colonne DateModification dans la table.
            /// </summary>
            public const string DateModification = "USER_MODIFICATION_DATE";

        }
        private const string SQLQUERY_SELECT_ALL_USER_PATTERN = "SELECT USER_ID,USER_LOGIN,USER_PWD,USER_NUMBER_LOGIN," +
            "USER_LAST_LOGIN,USER_CREATION_DATE,USER_MODIFICATION_DATE " +
            "FROM JC_USER";
        public static string SqlQuery_SelectAll()
        {
            return SQLQUERY_SELECT_ALL_USER_PATTERN;
        }

        private const string SQLQUERY_SELECT_USER_BY_LOGIN_PWD_PATTERN = "SELECT USER_ID, USER_LOGIN, USER_PWD, USER_NUMBER_LOGIN," +
            "USER_LAST_LOGIN,USER_CREATION_DATE,USER_MODIFICATION_DATE " +
            "FROM JC_USER "+
            "WHERE USER_LOGIN={0} AND USER_PWD={1}";
        public static string SqlQuery_SelectByLoginMdp(string login, string pwd)
        {
            QueryHelper helper = new QueryHelper();
            return string.Format(SQLQUERY_SELECT_USER_BY_LOGIN_PWD_PATTERN, helper.ConvertNullValues(login), helper.ConvertNullValues(pwd));
        }
    }
}
