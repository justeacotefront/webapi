﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApi.Assembler
{
    public static class OrderLineSqlTools
    {
        /// <summary>
        /// Description de la table JC_ORDERLINE.
        /// </summary>
        public static class OrderLineTable
        {
            /// <summary>
            /// Nom de ma colonne Id dans la table.
            /// </summary>
            public const string Id = "ORDERLINE_ID";

            /// <summary>
            /// Nom de la colonne IdOrder dans la table.
            /// </summary>
            public const string IdOrder = "ORDER_ID";

            /// <summary>
            /// Nom de la colonne IdProduct dans la table.
            /// </summary>
            public const string IdProduct = "PRODUCT_ID";

            /// <summary>
            /// Nom de la colonne Quantity dans la table.
            /// </summary>
            public const string Quantity = "ORDERLINE_QUANTITY";

            /// <summary>
            /// Nom de la colonne PrixHT dans la table.
            /// </summary>
            public const string PrixHT = "ORDERLINE_PRICE_HT";
            /// <summary>
            /// Nom de la colonne VAT dans la table.
            /// </summary>
            public const string VAT = "ORDERLINE_VAT";
            /// <summary>
            /// Nom de la colonne DateCreation dans la table.
            /// </summary>
            public const string DateCreation = "ORDERLINE_CREATION_DATE";

            /// <summary>
            /// Nom de la colonne DateModification dans la table.
            /// </summary>
            public const string DateModification = "ORDERLINE_MODIFICATION_DATE";
        }

        private const string SQLQUERY_INSERT_ORDERLINE_PATTERN = "INSERT INTO JC_ORDERLINE (ORDER_ID, PRODUCT_ID,ORDERLINE_QUANTITY,ORDERLINE_PRICE_HT,ORDERLINE_VAT,ORDERLINE_CREATION_DATE) VALUES ({0},{1},{2},{3},{4}, TO_DATE(sysdate))";
        public static string SqlQuery_Insert( int idOrder, int idProduct, int? quantity, float? prixHT, int? vat)
        {
            return string.Format(SQLQUERY_INSERT_ORDERLINE_PATTERN, idOrder, idProduct, quantity.HasValue ? quantity.Value.ToString() : "NULL", prixHT.HasValue ? prixHT.Value.ToString() : "NULL", vat.HasValue? vat.Value.ToString() : "NULL");
        }

        private const string SQLQUERY_SELECT_ALL_ORDERLINE_PATTERN = "SELECT ORDERLINE_ID,ORDER_ID, PRODUCT_ID,ORDERLINE_QUANTITY,ORDERLINE_PRICE_HT,ORDERLINE_VAT,ORDERLINE_CREATION_DATE, ORDERLINE_MODIFICATION_DATE FROM JC_ORDERLINE";
        public static string SqlQuery_SelectAll()
        {
            return SQLQUERY_SELECT_ALL_ORDERLINE_PATTERN;
        }

        private const string SQLQUERY_SELECT_ORDERLINE_BY_ID_PATTERN = "SELECT ORDERLINE_ID,ORDER_ID, PRODUCT_ID,ORDERLINE_QUANTITY,ORDERLINE_PRICE_HT,ORDERLINE_VAT,ORDERLINE_CREATION_DATE, ORDERLINE_MODIFICATION_DATE FROM JC_ORDERLINE WHERE ORDERLINE_ID = {0}";
        public static string SqlQuery_SelectById(int id)
        {
            return string.Format(SQLQUERY_SELECT_ORDERLINE_BY_ID_PATTERN, id);
        }

        private const string SQLQUERY_DELETE_ORDERLINE_BY_ID_PATTERN = "DELETE FROM JC_ORDERLINE WHERE ORDERLINE_ID = {0}";
        public static string SqlQuery_Delete(int id)
        {
            return string.Format(SQLQUERY_DELETE_ORDERLINE_BY_ID_PATTERN, id);
        }

        private const string SQLQUERY_UPDATE_ORDERLINE_PATTERN = "UPDATE JC_ORDERLINE  SET ORDER_ID = {0},PRODUCT_ID = {1},ORDERLINE_QUANTITY = {2},ORDERLINE_PRICE_HT = {3},ORDERLINE_VAT = {4}, ORDERLINE_MODIFICATION_DATE=TO_DATE(sysdate) WHERE ORDERLINE_ID = {5}";
        public static string SqlQuery_Update(int idOrder, int idProduct, int? quantity, float? prixHT, int? vat,int id)
        {
            return string.Format(SQLQUERY_UPDATE_ORDERLINE_PATTERN, idOrder, idProduct, quantity.HasValue ? quantity.Value.ToString() : "NULL", prixHT.HasValue ? prixHT.Value.ToString() : "NULL", vat.HasValue ? vat.Value.ToString() : "NULL", id);
        }

        private const string SQLQUERY_SELECT_ORDERLINE_BY_ID_ORDER_PATTERN = "SELECT ORDERLINE_ID,ORDER_ID, PRODUCT_ID,ORDERLINE_QUANTITY,ORDERLINE_PRICE_HT,ORDERLINE_VAT,ORDERLINE_CREATION_DATE, ORDERLINE_MODIFICATION_DATE FROM JC_ORDERLINE WHERE ORDER_ID = {0}";
        public static string SqlQuery_SelectByIdOrder(int id)
        {
            return string.Format(SQLQUERY_SELECT_ORDERLINE_BY_ID_ORDER_PATTERN, id);
        }
    }
}
