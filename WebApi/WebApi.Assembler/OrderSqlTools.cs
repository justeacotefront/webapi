﻿using System;
using WebApi.Assembler.Helper;

namespace WebApi.Assembler
{
    public static class OrderSqlTools
    {
        /// <summary>
        /// Description de la table JC_ORDER.
        /// </summary>
        public static class OrderTable
        {
            /// <summary>
            /// Nom de ma colonne Id dans la table.
            /// </summary>
            public const string Id = "ORDER_ID";

            /// <summary>
            /// Nom de la colonne IdCustomer dans la table.
            /// </summary>
            public const string IdCustomer = "CUSTOMER_ID";

            /// <summary>
            /// Nom de la colonne DateCreation dans la table.
            /// </summary>
            public const string DateCreation = "ORDER_CREATION_DATE";

            /// <summary>
            /// Nom de la colonne DateModification dans la table.
            /// </summary>
            public const string DateModification = "ORDER_MODIFICATION_DATE";

            /// <summary>
            /// Nom de la colonne Statut dans la table.
            /// </summary>
            public const string Statut = "ORDER_STATUS";

            /// <summary>
            /// Nom de la colonne ShopId dans la table.
            /// </summary>
            public const string ShopId = "SHOP_ID";

            /// <summary>
            /// Nom de la colonne RecoveryDate dans la table.
            /// </summary>
            public const string RecoveryDate = "ORDER_RECOVERY_DATE";

            /// <summary>
            /// Nom de la colonne ORDER_STATUS_DESC dans la table.
            /// </summary>
            public const string Description = "ORDER_STATUS_DESC";

        }

        private const string SQLQUERY_INSERT_ORDER_PATTERN = " BEGIN INSERT INTO JC_ORDER (CUSTOMER_ID, SHOP_ID,ORDER_RECOVERY_DATE,ORDER_CREATION_DATE) VALUES ({0}, {1},{2},TO_DATE(sysdate))RETURNING ORDER_ID INTO :id;END;";
        public static string SqlQuery_Insert(int idCustomer, int? idShop, DateTime? DateRecovery)
        {
            QueryHelper helper = new QueryHelper();
            return string.Format(SQLQUERY_INSERT_ORDER_PATTERN, idCustomer, helper.ConvertNullValues(idShop), helper.ConvertDateNullValues(DateRecovery));
        }

        private const string SQLQUERY_SELECT_ALL_ORDER_PATTERN = "SELECT ORDER_ID, CUSTOMER_ID, ORDER_CREATION_DATE,  ORDER_MODIFICATION_DATE, ORDER_STATUS, SHOP_ID, ORDER_RECOVERY_DATE, ORDER_STATUS_DESC FROM JC_ORDER";
        public static string SqlQuery_SelectAll()
        {
            return SQLQUERY_SELECT_ALL_ORDER_PATTERN;
        }

        private const string SQLQUERY_SELECT_ORDER_BY_ID_PATTERN = "SELECT ORDER_ID, CUSTOMER_ID, ORDER_CREATION_DATE, ORDER_MODIFICATION_DATE, ORDER_STATUS, SHOP_ID,ORDER_RECOVERY_DATE, ORDER_STATUS_DESC FROM JC_ORDER WHERE ORDER_ID = {0}";
        public static string SqlQuery_SelectById(int id)
        {
            return string.Format(SQLQUERY_SELECT_ORDER_BY_ID_PATTERN, id);
        }

        private const string SQLQUERY_DELETE_ORDER_BY_ID_PATTERN = "DELETE FROM JC_ORDER WHERE ORDER_ID = {0}";
        public static string SqlQuery_Delete(int id)
        {
            return string.Format(SQLQUERY_DELETE_ORDER_BY_ID_PATTERN, id);
        }

        private const string SQLQUERY_UPDATE_ORDER_PATTERN = "UPDATE JC_ORDER  SET CUSTOMER_ID = {0},ORDER_STATUS={1}, SHOP_ID={2}, ORDER_RECOVERY_DATE={3},ORDER_MODIFICATION_DATE=TO_DATE(sysdate), ORDER_STATUS_DESC={4} WHERE ORDER_ID = {5}";
        public static string SqlQuery_Update(int id, string statut, int idCustomer, int? idShop, DateTime? RecoveryDate, string description)
        {
            QueryHelper helper = new QueryHelper();
            if (string.IsNullOrEmpty(statut))
                statut = "En attente de validation";
            return string.Format(SQLQUERY_UPDATE_ORDER_PATTERN, idCustomer, helper.ConvertNullValues(statut), helper.ConvertNullValues(idShop), helper.ConvertDateNullValues(RecoveryDate), helper.ConvertNullValues(description), id);
        }

        private const string SQLQUERY_UPDATE_ORDER_STATUT_PATTERN = "UPDATE JC_ORDER  SET ORDER_STATUS='{0}', ORDER_MODIFICATION_DATE=TO_DATE(sysdate) WHERE ORDER_ID = {1}";
        public static string SqlQuery_Update_Status(int id, string statut)
        {
            if (string.IsNullOrEmpty(statut))
                statut = "En attente de validation";
            return string.Format(SQLQUERY_UPDATE_ORDER_STATUT_PATTERN, statut, id);
        }

        private const string SQLQUERY_SELECT_ORDER_BY_ID_SHOP_PATTERN = "SELECT ORDER_ID, CUSTOMER_ID, ORDER_CREATION_DATE, ORDER_MODIFICATION_DATE, ORDER_STATUS, SHOP_ID,ORDER_RECOVERY_DATE, ORDER_STATUS_DESC  FROM JC_ORDER WHERE SHOP_ID = {0}";
        public static string SqlQuery_SelectByIdShop(int idShop)
        {
            return string.Format(SQLQUERY_SELECT_ORDER_BY_ID_SHOP_PATTERN, idShop);
        }

        private const string SQLQUERY_SELECT_ORDER_BY_ID_CUSTOMER_PATTERN = "SELECT ORDER_ID, CUSTOMER_ID, ORDER_CREATION_DATE, ORDER_MODIFICATION_DATE, ORDER_STATUS, SHOP_ID,ORDER_RECOVERY_DATE, ORDER_STATUS_DESC  FROM JC_ORDER WHERE CUSTOMER_ID = {0}";
        public static string SqlQuery_SelectByIdCustomer(int idCustomer)
        {
            return string.Format(SQLQUERY_SELECT_ORDER_BY_ID_CUSTOMER_PATTERN, idCustomer);
        }

        private const string SQLQUERY_SELECT_CUSTOMER_ORDER_BY_ID_ORDER_PATTERN = "SELECT ORDER_ID, CUSTOMER_ID, ORDER_CREATION_DATE, ORDER_MODIFICATION_DATE, ORDER_STATUS, SHOP_ID,ORDER_RECOVERY_DATE  FROM JC_ORDER WHERE CUSTOMER_ID = {0} AND ORDER_ID = {1} ";
        public static string SqlQuery_SelectCustomerOrderByIdOrder(int idCustomer, int idOrder)
        {
            return string.Format(SQLQUERY_SELECT_CUSTOMER_ORDER_BY_ID_ORDER_PATTERN, idCustomer, idOrder);
        }
        private const string SQLQUERY_UPDATE_ORDER_DESCRIPTION_PATTERN = "UPDATE JC_ORDER  SET  ORDER_STATUS_DESC={0}, ORDER_MODIFICATION_DATE=TO_DATE(sysdate) WHERE ORDER_ID = {1}";
        public static string SqlQuery_Update_Description(int id, string description)
        {
            QueryHelper helper = new QueryHelper();
            return string.Format(SQLQUERY_UPDATE_ORDER_DESCRIPTION_PATTERN, helper.ConvertNullValues(description), id);
        }
    }
}
