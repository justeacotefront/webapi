﻿using System;
using Oracle.ManagedDataAccess.Client;
using WebApi.Transport;

namespace WebApi.Assembler
{
    public class SellerAssembler : BaseAssembler<Seller>
    {
        protected override void Populate(OracleDataReader source, Seller target)
        {
            if (source.HasRows)
            {
                int colIndex = source.GetOrdinal(SellerSqlTools.SellerTable.IdSeller);
                if (!source.IsDBNull(colIndex))
                    target.Identifiant = Convert.ToInt32(source[SellerSqlTools.SellerTable.IdSeller]);
                colIndex = source.GetOrdinal(SellerSqlTools.SellerTable.IdShop);
                if (!source.IsDBNull(colIndex))
                    target.ShopId = Convert.ToInt32(source[SellerSqlTools.SellerTable.IdShop]);
                colIndex = source.GetOrdinal(SellerSqlTools.SellerTable.FirstName);
                if (!source.IsDBNull(colIndex))
                    target.Prenom = source[SellerSqlTools.SellerTable.FirstName].ToString();
                colIndex = source.GetOrdinal(SellerSqlTools.SellerTable.LastName);
                if (!source.IsDBNull(colIndex))
                    target.Nom = source[SellerSqlTools.SellerTable.LastName].ToString();
                colIndex = source.GetOrdinal(SellerSqlTools.SellerTable.Phone);
                if (!source.IsDBNull(colIndex))
                    target.Telephone = source[SellerSqlTools.SellerTable.Phone].ToString() ;
                colIndex = source.GetOrdinal(SellerSqlTools.SellerTable.Email);
                if (!source.IsDBNull(colIndex))
                    target.Email = source[SellerSqlTools.SellerTable.Email].ToString();
                colIndex = source.GetOrdinal(SellerSqlTools.SellerTable.Function);
                if (!source.IsDBNull(colIndex))
                    target.Function = source[SellerSqlTools.SellerTable.Function].ToString();
                colIndex = source.GetOrdinal(SellerSqlTools.SellerTable.DateCreation);
                if (!source.IsDBNull(colIndex))
                    target.Creation = Convert.ToDateTime(source[SellerSqlTools.SellerTable.DateCreation]);
                colIndex = source.GetOrdinal(SellerSqlTools.SellerTable.CreatedBy);
                if (!source.IsDBNull(colIndex))
                    target.CreatedBy = Convert.ToInt32(source[SellerSqlTools.SellerTable.CreatedBy]);
                colIndex = source.GetOrdinal(SellerSqlTools.SellerTable.DateModification);
                if (!source.IsDBNull(colIndex))
                    target.Modification = Convert.ToDateTime(source[SellerSqlTools.SellerTable.DateModification]);
                colIndex = source.GetOrdinal(SellerSqlTools.SellerTable.UserId);
                if (!source.IsDBNull(colIndex))
                    target.UserId = Convert.ToInt32(source[SellerSqlTools.SellerTable.UserId]);
                colIndex = source.GetOrdinal(SellerSqlTools.SellerTable.IdPicture);
                if (!source.IsDBNull(colIndex))
                    target.IdImage = Convert.ToInt32(source[SellerSqlTools.SellerTable.IdPicture]);
                colIndex = source.GetOrdinal(SellerSqlTools.SellerTable.PicturePath);
                if (!source.IsDBNull(colIndex))
                    target.PicturePath = source[SellerSqlTools.SellerTable.PicturePath].ToString();

            }
        }
    }
}
