﻿using Oracle.ManagedDataAccess.Client;
using System;
using WebApi.Transport;

namespace WebApi.Assembler
{
    public class ProductCategoryAssembler : BaseAssembler<ProductCategory>
    {
        protected override void Populate(OracleDataReader source, ProductCategory target)
        {
            if (source.HasRows)
            {
                int colIndex = source.GetOrdinal(ProductCategorySqlTools.ProductCategoryTable.CategoryId);
                if (!source.IsDBNull(colIndex))
                    target.CategoryId = Convert.ToInt32(source[ProductCategorySqlTools.ProductCategoryTable.CategoryId]);
                colIndex = source.GetOrdinal(ProductCategorySqlTools.ProductCategoryTable.ProductId);
                if (!source.IsDBNull(colIndex))
                    target.ProductId = Convert.ToInt32(source[ProductCategorySqlTools.ProductCategoryTable.ProductId]);
            }
        }
    }
}
