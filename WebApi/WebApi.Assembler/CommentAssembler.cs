﻿using Oracle.ManagedDataAccess.Client;
using System;
using WebApi.Transport;

namespace WebApi.Assembler
{
    public class CommentAssembler : BaseAssembler<Comment>
    {
        protected override void Populate(OracleDataReader source, Comment target)
        {
            if (source.HasRows)
            {
                int colIndex = source.GetOrdinal(CommentSqlTools.CommentTable.Id);
                if (!source.IsDBNull(colIndex))
                    target.Identifiant = Convert.ToInt32(source[CommentSqlTools.CommentTable.Id]);
                colIndex = source.GetOrdinal(CommentSqlTools.CommentTable.IdCustomer);
                if (!source.IsDBNull(colIndex))
                    target.CustomerId = Convert.ToInt32(source[CommentSqlTools.CommentTable.IdCustomer]);
                colIndex = source.GetOrdinal(CommentSqlTools.CommentTable.IdProduct);
                if (!source.IsDBNull(colIndex))
                    target.ProductId = Convert.ToInt32(source[CommentSqlTools.CommentTable.IdProduct]);
                colIndex = source.GetOrdinal(CommentSqlTools.CommentTable.Titre);
                if (!source.IsDBNull(colIndex))
                    target.Titre = source[CommentSqlTools.CommentTable.Titre].ToString() ;
                colIndex = source.GetOrdinal(CommentSqlTools.CommentTable.Description);
                if (!source.IsDBNull(colIndex))
                    target.Description = source[CommentSqlTools.CommentTable.Description].ToString();
                colIndex = source.GetOrdinal(CommentSqlTools.CommentTable.DateCreation);
                if (!source.IsDBNull(colIndex))
                    target.Creation = Convert.ToDateTime(source[CommentSqlTools.CommentTable.DateCreation]);
                colIndex = source.GetOrdinal(CommentSqlTools.CommentTable.DateModification);
                if (!source.IsDBNull(colIndex))
                    target.Modification = Convert.ToDateTime(source[CommentSqlTools.CommentTable.DateModification]);


            }

        }

    }
}
