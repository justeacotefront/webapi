﻿namespace WebApi.Assembler
{
    public class ShopCategorySqlTools
    {
        /// <summary>
        /// Description de la table JC_SHOP_CATEGORY.
        /// </summary>
        public static class ShopCategoryTable
        {
            /// <summary>
            /// Nom de ma colonne CategoryId dans la table.
            /// </summary>
            public const string CategoryId = "CATEGORY_ID";

            /// <summary>
            /// Nom de ma colonne ShopId dans la table.
            /// </summary>
            public const string ShopId = "SHOP_ID";

        }

        private const string SQLQUERY_INSERT_SHOP_CATEGORY_PATTERN = "INSERT INTO JC_SHOP_CATEGORY (CATEGORY_ID, SHOP_ID) VALUES ({0},{1})";
        public static string SqlQuery_Insert(int CategoryId, int ShopId)
        {
            return string.Format(SQLQUERY_INSERT_SHOP_CATEGORY_PATTERN, CategoryId, ShopId);
        }

        private const string SQLQUERY_SELECT_ALL_SHOP_CATEGORY_PATTERN = "SELECT CATEGORY_ID, SHOP_ID FROM JC_SHOP_CATEGORY";
        public static string SqlQuery_SelectAll()
        {
            return SQLQUERY_SELECT_ALL_SHOP_CATEGORY_PATTERN;
        }

        private const string SQLQUERY_SELECT_SHOP_CATEGORY_BY_IDSHOP_PATTERN = "SELECT CATEGORY_ID, SHOP_ID FROM JC_SHOP_CATEGORY WHERE SHOP_ID = {0}";
        public static string SqlQuery_SelectByIdProduct(int ShopId)
        {
            return string.Format(SQLQUERY_SELECT_SHOP_CATEGORY_BY_IDSHOP_PATTERN, ShopId);
        }
        private const string SQLQUERY_SELECT_SHOP_CATEGORY_BY_IDCATEGORY_PATTERN = "SELECT CATEGORY_ID, SHOP_ID FROM JC_SHOP_CATEGORY WHERE CATEGORY_ID = {0}";
        public static string SqlQuery_SelectByIdCategory(int CategoryId)
        {
            return string.Format(SQLQUERY_SELECT_SHOP_CATEGORY_BY_IDCATEGORY_PATTERN, CategoryId);
        }

        private const string SQLQUERY_DELETE_SHOP_CATEGORY_BY_ID_PATTERN = "DELETE FROM JC_SHOP_CATEGORY WHERE CATEGORY_ID = {0} AND SHOP_ID={1}";
        public static string SqlQuery_Delete(int CategoryId, int ShopId)
        {
            return string.Format(SQLQUERY_DELETE_SHOP_CATEGORY_BY_ID_PATTERN, CategoryId, ShopId);
        }

    }
}
