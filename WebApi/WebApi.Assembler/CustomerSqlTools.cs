﻿
using WebApi.Assembler.Helper;

namespace WebApi.Assembler
{
    public static class CustomerSqlTools
    {
        /// <summary>
        /// Description de la table JC_CUSTOMER.
        /// </summary>
        public static class CustomerTable
        {
            /// <summary>
            /// Nom de ma colonne Id dans la table.
            /// </summary>
            public const string Id = "CUSTOMER_ID";

            /// <summary>
            /// Nom de la colonne FirstName dans la table.
            /// </summary>
            public const string FirstName = "CUSTOMER_FIRSTNAME";

            /// <summary>
            /// Nom de la colonne LastName dans la table.
            /// </summary>
            public const string LastName = "CUSTOMER_LASTNAME";

            /// <summary>
            /// Nom de la colonne Phone dans la table.
            /// </summary>
            public const string Phone = "CUSTOMER_PHONE";

            /// <summary>
            /// Nom de la colonne Email dans la table.
            /// </summary>
            public const string Email = "CUSTOMER_EMAIL";

            /// <summary>
            /// Nom de la colonne Rue dans la table.
            /// </summary>
            public const string Rue = "CUSTOMER_STREET";

            /// <summary>
            /// Nom de la colonne CodePostal dans la table.
            /// </summary>
            public const string CodePostal = "CUSTOMER_POSTAL_CODE";

            /// <summary>
            /// Nom de la colonne Ville dans la table.
            /// </summary>
            public const string Ville = "CUSTOMER_CITY";

            /// <summary>
            /// Nom de la colonne DateCreation dans la table.
            /// </summary>
            public const string DateCreation = "CUSTOMER_CREATION_DATE";

            /// <summary>
            /// Nom de la colonne DateModification dans la table.
            /// </summary>
            public const string DateModification = "CUSTOMER_MODIFICATION_DATE";

            /// <summary>
            /// Nom de la colonne UserId dans la table.
            /// </summary>
            public const string UserId = "USER_ID";

            /// <summary>
            /// Nom de la colonne Disabled dans la table.
            /// </summary>
            public const string Disabled = "CUSTOMER_DISABLED";

            /// <summary>
            /// Nom de la colonne CUSTOMER_PICTURE_ID dans la table.
            /// </summary>
            public const string PictureId = "CUSTOMER_PICTURE_ID";

            /// <summary>
            /// Nom de la colonne PicturePath dans la table
            /// </summary>
            public const string PicturePath = "PICUTRE_PATH";
        }

        private const string SQLQUERY_INSERT_CUSTOMER_PATTERN = "INSERT INTO JC_CUSTOMER " +
            "(CUSTOMER_FIRSTNAME,CUSTOMER_LASTNAME,CUSTOMER_PHONE,CUSTOMER_EMAIL,CUSTOMER_STREET," +
            " CUSTOMER_POSTAL_CODE,CUSTOMER_CITY,USER_ID, CUSTOMER_CREATION_DATE, CUSTOMER_ID, PICUTRE_PATH) " +
            "VALUES " +
            "({0},{1},{2},{3},{4},{5},{6},{7}, TO_DATE(sysdate),{8}, {9})";
        public static string SqlQuery_Insert(string firstname, string lastname, string phone, string email, string street, string codePostal, string ville,
             int? userId, int identifiant, string picturePath)
        {
            QueryHelper helper = new QueryHelper();
            return string.Format(SQLQUERY_INSERT_CUSTOMER_PATTERN, helper.ConvertNullValues(firstname), helper.ConvertNullValues(lastname), helper.ConvertNullValues(phone), helper.ConvertNullValues(email),
                helper.ConvertNullValues(street), helper.ConvertNullValues(codePostal), helper.ConvertNullValues(ville), helper.ConvertNullValues(userId), helper.ConvertNullValues(identifiant), helper.ConvertNullValues(picturePath));
        }

        private const string SQLQUERY_SELECT_ALL_CUSTOMER_PATTERN = "SELECT CUSTOMER_ID,CUSTOMER_FIRSTNAME,CUSTOMER_LASTNAME,CUSTOMER_PHONE,CUSTOMER_EMAIL,CUSTOMER_STREET," +
            " CUSTOMER_POSTAL_CODE,CUSTOMER_CITY,USER_ID,CUSTOMER_CREATION_DATE,CUSTOMER_MODIFICATION_DATE, CUSTOMER_DISABLED, CUSTOMER_PICTURE_ID, PICUTRE_PATH " +
            " FROM JC_CUSTOMER";
        public static string SqlQuery_SelectAll()
        {
            return SQLQUERY_SELECT_ALL_CUSTOMER_PATTERN;
        }

        private const string SQLQUERY_SELECT_CUSTOMER_BY_ID_PATTERN = "SELECT CUSTOMER_ID,CUSTOMER_FIRSTNAME,CUSTOMER_LASTNAME,CUSTOMER_PHONE,CUSTOMER_EMAIL,CUSTOMER_STREET," +
            " CUSTOMER_POSTAL_CODE,CUSTOMER_CITY,USER_ID,CUSTOMER_CREATION_DATE,CUSTOMER_MODIFICATION_DATE, CUSTOMER_DISABLED, CUSTOMER_PICTURE_ID,PICUTRE_PATH " +
            " FROM JC_CUSTOMER WHERE CUSTOMER_ID = {0}";
        public static string SqlQuery_SelectById(int id)
        {
            return string.Format(SQLQUERY_SELECT_CUSTOMER_BY_ID_PATTERN, id);
        }

        private const string SQLQUERY_DELETE_SELLER_BY_ID_PATTERN = "DELETE FROM JC_CUSTOMER WHERE CUSTOMER_ID = {0}";
        public static string SqlQuery_Delete(int id)
        {
            return string.Format(SQLQUERY_DELETE_SELLER_BY_ID_PATTERN, id);
        }

        private const string SQLQUERY_UPDATE_CUSTOMER_PATTERN = "UPDATE JC_CUSTOMER  SET " +
            "CUSTOMER_FIRSTNAME={0},CUSTOMER_LASTNAME={1},CUSTOMER_PHONE={2},CUSTOMER_EMAIL={3}," +
            "CUSTOMER_STREET={4},CUSTOMER_POSTAL_CODE={5},CUSTOMER_CITY={6},USER_ID={7}, PICUTRE_PATH = {8}," +
            "CUSTOMER_MODIFICATION_DATE=TO_DATE(sysdate) " +
            "WHERE CUSTOMER_ID = {9}";
        public static string SqlQuery_Update(string firstname, string lastname, string phone, string email, string street,
            string codePostal, string ville, int? userId, string picturePath,int id)
        {
            QueryHelper helper = new QueryHelper();
            return string.Format(SQLQUERY_UPDATE_CUSTOMER_PATTERN, helper.ConvertNullValues(firstname), helper.ConvertNullValues(lastname), helper.ConvertNullValues(phone), helper.ConvertNullValues(email),
                helper.ConvertNullValues(street), helper.ConvertNullValues(codePostal), helper.ConvertNullValues(ville), helper.ConvertNullValues(userId), helper.ConvertNullValues(picturePath), id);
        }

        private const string SQLQUERY_UPDATE_CUSTOMER_MAIL_PATTERN = "UPDATE JC_CUSTOMER  SET " +
        "CUSTOMER_EMAIL={0}," +
        "CUSTOMER_MODIFICATION_DATE=TO_DATE(sysdate) " +
        "WHERE CUSTOMER_ID = {1}";
        public static string SqlQuery_Update_Email(string email, int id)
        {
            QueryHelper helper = new QueryHelper();
            return string.Format(SQLQUERY_UPDATE_CUSTOMER_MAIL_PATTERN, helper.ConvertNullValues(email), id);
        }

        private const string SQLQUERY_UPDATE_CUSTOMER_ACCOUNT_PATTERN = "UPDATE JC_CUSTOMER  SET " +
        " CUSTOMER_DISABLED={0}," +
        " CUSTOMER_MODIFICATION_DATE=TO_DATE(sysdate) " +
        " WHERE CUSTOMER_ID = {1}";
        public static string SqlQuery_Update_Account(int disabled, int id)
        {
            QueryHelper helper = new QueryHelper();
            return string.Format(SQLQUERY_UPDATE_CUSTOMER_ACCOUNT_PATTERN, disabled, id);
        }

        private const string SQLQUERY_INSERT_CUSTOMER_PICTURE_PATH_PATTERN = "UPDATE JC_CUSTOMER  SET " +
           "PICUTRE_PATH = {0}" +
           "WHERE CUSTOMER_ID = {1}";
        public static string SqlQuery_InsertPicturePath(string picturePath, int id)
        {
            QueryHelper helper = new QueryHelper();
            return string.Format(SQLQUERY_INSERT_CUSTOMER_PICTURE_PATH_PATTERN, helper.ConvertNullValues(picturePath), id);
        }
    }
}
