﻿using System;
using Oracle.ManagedDataAccess.Client;
using WebApi.Transport;

namespace WebApi.Assembler
{
    public class CustomerAssembler : BaseAssembler<Customer>
    {
        protected override void Populate(OracleDataReader source, Customer target)
        {
            if (source.HasRows)
            {
                int colIndex = source.GetOrdinal(CustomerSqlTools.CustomerTable.Id);
                if (!source.IsDBNull(colIndex))
                    target.Identifiant = Convert.ToInt32(source[CustomerSqlTools.CustomerTable.Id]);
                colIndex = source.GetOrdinal(CustomerSqlTools.CustomerTable.FirstName);
                if (!source.IsDBNull(colIndex))
                    target.Prenom = source[CustomerSqlTools.CustomerTable.FirstName].ToString();
                colIndex = source.GetOrdinal(CustomerSqlTools.CustomerTable.LastName);
                if (!source.IsDBNull(colIndex))
                    target.Nom = source[CustomerSqlTools.CustomerTable.LastName].ToString();
                colIndex = source.GetOrdinal(CustomerSqlTools.CustomerTable.Phone);
                if (!source.IsDBNull(colIndex))
                    target.Telephone = source[CustomerSqlTools.CustomerTable.Phone].ToString();
                colIndex = source.GetOrdinal(CustomerSqlTools.CustomerTable.Email);
                if (!source.IsDBNull(colIndex))
                    target.Email = source[CustomerSqlTools.CustomerTable.Email].ToString();
                colIndex = source.GetOrdinal(CustomerSqlTools.CustomerTable.Rue);
                if (!source.IsDBNull(colIndex))
                    target.Rue = source[CustomerSqlTools.CustomerTable.Rue].ToString();
                colIndex = source.GetOrdinal(CustomerSqlTools.CustomerTable.CodePostal);
                if (!source.IsDBNull(colIndex))
                    target.CP = source[CustomerSqlTools.CustomerTable.CodePostal].ToString();
                colIndex = source.GetOrdinal(CustomerSqlTools.CustomerTable.Ville);
                if (!source.IsDBNull(colIndex))
                    target.Ville = source[CustomerSqlTools.CustomerTable.Ville].ToString();
                colIndex = source.GetOrdinal(CustomerSqlTools.CustomerTable.DateCreation);
                if (!source.IsDBNull(colIndex))
                    target.DateCreation = Convert.ToDateTime(source[CustomerSqlTools.CustomerTable.DateCreation]);
                colIndex = source.GetOrdinal(CustomerSqlTools.CustomerTable.DateModification);
                if (!source.IsDBNull(colIndex))
                    target.Modification = Convert.ToDateTime(source[CustomerSqlTools.CustomerTable.DateModification]);
                colIndex = source.GetOrdinal(CustomerSqlTools.CustomerTable.UserId);
                if (!source.IsDBNull(colIndex))
                    target.UserId = Convert.ToInt32(source[CustomerSqlTools.CustomerTable.UserId]);
                colIndex = source.GetOrdinal(CustomerSqlTools.CustomerTable.Disabled);
                if (!source.IsDBNull(colIndex))
                    target.Disabled = Convert.ToInt32(source[CustomerSqlTools.CustomerTable.Disabled]) == 0 ? false : true;
                colIndex = source.GetOrdinal(CustomerSqlTools.CustomerTable.PictureId);
                if (!source.IsDBNull(colIndex))
                    target.IdImage = Convert.ToInt32(source[CustomerSqlTools.CustomerTable.PictureId]);
                colIndex = source.GetOrdinal(CustomerSqlTools.CustomerTable.PicturePath);
                if (!source.IsDBNull(colIndex))
                    target.PicturePath = source[CustomerSqlTools.CustomerTable.PicturePath].ToString();
            }
        }
    }
}
