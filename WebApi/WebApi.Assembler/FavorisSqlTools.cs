﻿namespace WebApi.Assembler
{
    public static class FavorisSqlTools
    {
        /// <summary>
        /// Description de la table JC_FAVORITE.
        /// </summary>
        public static class FavorisTable
        {
            /// <summary>
            /// Nom de ma colonne ShopId dans la table.
            /// </summary>
            public const string ShopId = "SHOP_ID";

            /// <summary>
            /// Nom de la colonne CustomerId dans la table.
            /// </summary>
            public const string CustomerId = "CUSTOMER_ID";

            /// <summary>
            /// Nom de la colonne Creation dans la table.
            /// </summary>
            public const string Creation = "FAVORITE_CREATION_DATE";
        }

        private const string SQLQUERY_INSERT_FAVORIS_PATTERN = "INSERT INTO JC_FAVORITE (SHOP_ID, CUSTOMER_ID,FAVORITE_CREATION_DATE) VALUES ({0},{1}, TO_DATE(sysdate))";
        public static string SqlQuery_Insert(int ShopId, int CustomerId)
        {
            return string.Format(SQLQUERY_INSERT_FAVORIS_PATTERN, ShopId, CustomerId);
        }

        private const string SQLQUERY_DELETE_FAVORIS_BY_ID_PATTERN = "DELETE FROM JC_FAVORITE WHERE SHOP_ID = {0} AND CUSTOMER_ID={1}";
        public static string SqlQuery_Delete(int ShopId, int CustomerId)
        {
            return string.Format(SQLQUERY_DELETE_FAVORIS_BY_ID_PATTERN, ShopId, CustomerId);
        }

        private const string SQLQUERY_SELECT_FAVORIS_BY_IDCUSTOMER_PATTERN = "SELECT SHOP_ID, CUSTOMER_ID,FAVORITE_CREATION_DATE FROM JC_FAVORITE WHERE CUSTOMER_ID = {0}";
        public static string SqlQuery_SelectByIdCustomer(int CustomerId)
        {
            return string.Format(SQLQUERY_SELECT_FAVORIS_BY_IDCUSTOMER_PATTERN, CustomerId);
        }

        private const string SQLQUERY_SELECT_FAVORIS_BY_IDCUSTOMER_SHOP_PATTERN = "SELECT SHOP_ID, CUSTOMER_ID,FAVORITE_CREATION_DATE FROM JC_FAVORITE WHERE SHOP_ID = {0} AND CUSTOMER_ID = {1} ";
        public static string SqlQuery_SelectByIdCustomerAndShop(int ShopId, int CustomerId)
        {
            return string.Format(SQLQUERY_SELECT_FAVORIS_BY_IDCUSTOMER_SHOP_PATTERN, ShopId, CustomerId);
        }
    }
}