﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.DataAccess;
using WebApi.Transport;

namespace WebApi.Controllers
{
    public class AdministrateurController : Controller
    {
        // GET: Administrateur
        public ActionResult Index()
        {
            return View();
        }

        // GET: Administrateur/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Administrateur/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Administrateur/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Administrateur/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Administrateur/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Administrateur/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Administrateur/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        /// <summary>
        /// Permet de recuperer la liste des consommateurs
        /// </summary>
        /// <returns></returns>
        //GET /api/customer
        [HttpGet]
        public List<Customer> GetCustomer()
        {
            CustomerDao customerDao = new CustomerDao();
            return customerDao.GetAll();
        }
    }
}