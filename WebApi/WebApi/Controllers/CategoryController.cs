﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WebApi.DataAccess;
using WebApi.Transport;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class CategoryController : Controller
    {
        /// <summary>
        /// Permet de récuperer toutes les categories s
        /// </summary>
        /// <returns>Listes de categorie</returns>
        // GET: api/category
        [HttpGet]
        public List<Category> GetAll()
        {
            CategoryDao categoryDao = new CategoryDao();
            return categoryDao.GetAll();
        }

        /// <summary>
        /// Permet de récuperer la liste une catégorie par son identifiant
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/category/5
        [HttpGet("{id}")]
        public Category GetById(int id)
        {
            CategoryDao categoryDao = new CategoryDao();
            return categoryDao.GetById(id);
        }

        /// <summary>
        /// Permet de crée une categorie
        /// </summary>
        /// <param name="newCategory"></param>
        // POST api/category
        [HttpPost]
        public void Create([FromBody] Category newCategory)
        {
            if (newCategory == null)
                throw new Exception();
            CategoryDao categoryDao = new CategoryDao();
            categoryDao.Create(newCategory);

        }

        /// <summary>
        /// Permet de mettre à jour une categorie
        /// </summary>
        /// <param name="newCategory"></param>
        /// <returns></returns>
        // PUT api/category
        [HttpPut]
        public IActionResult Update([FromBody]Category newCategory)
        {
            CategoryDao categoryDao = new CategoryDao();
            Category category = categoryDao.GetById(newCategory.Identifiant);
            if (category == null)
            {
                return NotFound();
            }
            categoryDao.Update(newCategory);
            return Ok();
        }

        /// <summary>
        /// Suppression d'une categorie
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE api/category/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            CategoryDao categoryDao = new CategoryDao();
            Category category = categoryDao.GetById(id);
            if (category == null)
            {
                return NotFound();
            }
            categoryDao.Delete(category.Identifiant);
            return Ok();
        }

    }
}
