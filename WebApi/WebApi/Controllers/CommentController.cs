﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WebApi.DataAccess;
using WebApi.Transport;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class CommentController : Controller
    {
        /// <summary>
        /// Permet de récuperer la liste des commentaires
        /// </summary>
        /// <returns>liste de commentaire</returns>
        // GET: api/comment
        [HttpGet]
        public List<Comment> GetAll()
        {
            CommentDao commentDAO = new CommentDao();
            return commentDAO.GetAll();
        }

        /// <summary>
        /// Permet de récuperer un commentaire selon son identifiant 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/comment/5
        [HttpGet("{id}")]
        public Comment Get(int id)
        {
            CommentDao commentDAO = new CommentDao();
            return commentDAO.GetById(id);
        }

        /// <summary>
        /// Permet de récuperer un commentaire selon son identifiant 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/comment/5
        [HttpGet("GetByIdProduct/{id}")]
        public List<Comment> GetByIdProduct(int id)
        {
            CommentDao commentDAO = new CommentDao();
            return commentDAO.GetByIdProduct(id);
        }


        /// <summary>
        /// Permet de créer un commentaire
        /// </summary>
        /// <param name="comment"></param>
        // POST api/comment
        [HttpPost]
        public void Create([FromBody]Comment comment)
        {
            if (comment == null)
                throw new System.Exception(); 
            CommentDao commentDAO = new CommentDao();
            commentDAO.Create(comment);
        }

        /// <summary>
        /// Permet de modifier un commentaire
        /// </summary>
        /// <param name="comment"></param>
        // PUT api/comment
        [HttpPut]
        public void Update([FromBody]Comment comment)
        {
            if (comment == null)
                throw new System.Exception();
            CommentDao commentDAO = new CommentDao();
            Comment oldComment = commentDAO.GetById(comment.Identifiant);
            if (oldComment == null)
                throw new System.Exception();
            commentDAO.Update(comment);
        }

        /// <summary>
        /// Permet de supprimer un commentaire
        /// </summary>
        /// <param name="id"></param>
        // DELETE api/comment/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            CommentDao commentDAO = new CommentDao();
            commentDAO.Delete(id);
        }
    }
}
