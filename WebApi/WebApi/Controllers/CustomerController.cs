﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using WebApi.DataAccess;
using WebApi.ServicesMetiers;
using WebApi.Transport;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        /// <summary>
        /// Permet de récupérer un consommateur selon son identifiant
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ActionResult<Customer> GetCustomerById(int id)
        {
            Customer customer = new Customer();
            try
            {
                CustomersServicesMetiers servicesMetiers = new CustomersServicesMetiers();
                customer = servicesMetiers.GetCustomerById(id);
                if (customer.IdImage.HasValue)
                {
                    //On récupére l'image
                    PicturesDao pictureDao = new PicturesDao();
                    customer.Image = pictureDao.GetById(customer.IdImage.Value);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }
            return customer;

        }

        /// <summary>
        /// Recupere la liste de toutes les commandes 
        /// </summary>
        /// <returns></returns>
        // GET: api/customer/GetAllOrder/2
        [HttpGet("GetAllOrders/{idCustomer}")]
        public List<Order> GetAllOrders(int idCustomer)
        {
            List<Order> customerOrders = new List<Order>();
            try
            {
                CustomersServicesMetiers servicesMetiers = new CustomersServicesMetiers();
                customerOrders = servicesMetiers.GetAllOrders(idCustomer);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }
            return customerOrders;

        }

        /// <summary>
        /// Permet de créer un consommateur
        /// </summary>
        /// <param name="customer"></param>
        [HttpPost]
        public bool Create([FromBody]Customer customer)
        {
            try
            {
                CustomersServicesMetiers servicesMetiers = new CustomersServicesMetiers();
                return servicesMetiers.CreerCompteCustomer(customer);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }
            return false;
        }

        /// <summary>
        /// Permet de mettre à jour un consommateur
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        [HttpPut]
        public bool Update([FromBody]Customer customer)
        {
            try
            {
                CustomersServicesMetiers servicesMetiers = new CustomersServicesMetiers();
                return servicesMetiers.UpdateInfosCustomer(customer);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }
            return false;

        }

        /// <summary>
        /// Met à jour l'email de l'utilisateur
        /// </summary>
        /// <param name="email"></param>
        /// <param name="identifiant"></param>
        [HttpPut("UpdateEmail/{identifiant}")]
        public bool UpdateEmail(int identifiant, [FromBody]string email)
        {
            bool result = false;
            try
            {
                CustomerDao customerDao = new CustomerDao();
                Customer customerExistant = customerDao.GetById(identifiant);
                if (customerExistant == null || string.IsNullOrEmpty(email))
                {
                    throw new System.Exception();
                }
                result = customerDao.UpdateEmail(identifiant, email);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }

            return result;
        }

        /// <summary>
        /// Met à jour le status du compte utilisateur
        /// Actif/Desactivé
        /// </summary>
        /// <param name="email"></param>
        /// <param name="identifiant"></param>
        [HttpPut("GererAccount/{identifiant}")]
        public ActionResult<bool> GererAccount(int identifiant)
        {
            bool result = false;
            try
            {
                CustomersServicesMetiers servicesMetiers = new CustomersServicesMetiers();
                result = servicesMetiers.UpdateStatusAccount(identifiant);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }
            return result;

        }


        /// <summary>
        /// Permet de créer une commande pour un utilisateur
        /// </summary>
        /// <param name="newOrder"></param>
        // POST api/customer/CreateOrder
        [HttpPost("CreateOrder")]
        public void CreateOrder([FromBody] Order newOrder)
        {
            try
            {
                if (newOrder == null)
                    throw new System.Exception();
                CustomersServicesMetiers customersServicesMetiers = new CustomersServicesMetiers();
                customersServicesMetiers.CreerOrder(newOrder);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }

        }


        /// <summary>
        /// Crée un nouvelle image lié à un client
        /// </summary>
        /// <param name="picture"></param>
        // POST api/product
        [HttpPost("AddPicture/{idCustomer}")]
        public void AddPicture(int idCustomer, [FromBody]Picture picture)
        {

            try
            {
                //On verifie que notre objet image n'est pas null
                if (picture == null)
                    throw new System.Exception();
                //On verifie que l'id correspond bien à un client
                CustomersServicesMetiers servicesMetiers = new CustomersServicesMetiers();
                Customer customer = servicesMetiers.GetCustomerById(idCustomer);
                if (customer == null)
                    throw new System.Exception();
                //On crée l'image et on récupére la clé primaire de l'objet
                servicesMetiers.AjoutImage(customer, picture); 
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }
        }

        /// <summary>
        ///  Permet de mettre à jour le chemin de l'image du client
        /// </summary>
        /// <param name="idCustomer"></param>
        /// <param name="picturePath"></param>
        // get api/shop/UpdatePicture
        [HttpGet("UpdatePicture/{idCustomer}/{picturePath}")]
        public void UpdatePicture(int idCustomer, string picturePath)
        {
            CustomerDao customerDAO = new CustomerDao();
            Customer oldCustomer = customerDAO.GetById(idCustomer);
            if (oldCustomer == null)
                throw new System.Exception();
            customerDAO.UpdatePicturePath(idCustomer, picturePath);
        }

    }
}