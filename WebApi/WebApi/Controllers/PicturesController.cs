﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WebApi.DataAccess;
using WebApi.Transport;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class PicturesController : Controller
    {
        /// <summary>
        /// Permet de récuperer la liste des images
        /// </summary>
        /// <returns>liste de images</returns>
        // GET: api/pictures
        [HttpGet]
        public List<Picture> GetAll()
        {
            List<Picture> images = new List<Picture>();
            try
            {
                PicturesDao picturesDao = new PicturesDao();
                images = picturesDao.GetAll();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }
            return images;

        }

        /// <summary>
        /// Permet de récuperer une image selon son identifiant 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/pictures/5
        [HttpGet("{id}")]
        public Picture Get(int id)
        {
            Picture image = new Picture();
            try
            {
                PicturesDao pictureDao = new PicturesDao();
                image = pictureDao.GetById(id);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }
            return image;
        }

        /// <summary>
        /// Permet de créer une image
        /// </summary>
        /// <param name="image"></param>
        // POST api/pictures
        [HttpPost]
        public void Create([FromBody]Picture image)
        {
            try
            {
                if (image == null)
                    throw new System.Exception();

                PicturesDao pictureDao = new PicturesDao();
                pictureDao.Create(image);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }
        }

        /// <summary>
        /// Permet de modifier une image 
        /// </summary>
        /// <param name="image"></param>
        // PUT api/pictures
        [HttpPut]
        public void Update([FromBody]Picture image)
        {
            try
            {
                if (image == null)
                    throw new System.Exception();

                PicturesDao pictureDao = new PicturesDao();
                Picture oldPicture = pictureDao.GetById(image.Identifiant);

                if (oldPicture == null)
                    throw new System.Exception();
                pictureDao.Update(image);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }
        }

        /// <summary>
        /// Permet de supprimer une image
        /// </summary>
        /// <param name="id"></param>
        // DELETE api/pictures/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            try
            {
                PicturesDao pictureDao = new PicturesDao();
                Picture oldPicture = pictureDao.GetById(id);

                if (oldPicture == null)
                    throw new System.Exception();

                pictureDao.Delete(id);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }
        }
    }
}
