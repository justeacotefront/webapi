﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WebApi.DataAccess;
using WebApi.Transport;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class ProductCategoryController : Controller
    {
        /// <summary>
        /// Permet de récuperer la liste de toutes les categories des produits
        /// </summary>
        /// <returns></returns>
        // GET: api/productcategory
        [HttpGet]
        public List<ProductCategory> GetAll()
        {
            ProductCategoryDao productCategoryDao = new ProductCategoryDao();
            return productCategoryDao.GetAll();
        }

        /// <summary>
        /// permet de récuperer une categorie selon l'identifiant d'un produit
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/productcategory/GetByProductId/5
        [HttpGet("GetByProductId/{id}")]
        public List<ProductCategory> GetByProductId(int id)
        {
            ProductCategoryDao productCategoryDao = new ProductCategoryDao();
            return productCategoryDao.GetByProductId(id);
        }

        /// <summary>
        /// permet de recuperer une categorie selon son identifiant
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/productcategory/GetByCategoryId/5
        [HttpGet("GetByCategoryId/{id}")]
        public List<ProductCategory> GetByCategoryId(int id)
        {
            ProductCategoryDao productCategoryDao = new ProductCategoryDao();
            return productCategoryDao.GetByCategoryId(id);
        }

        /// <summary>
        /// Permet de créer une nouvelle categorie
        /// </summary>
        /// <param name="newProductCategory"></param>
        // POST api/<controller>
        [HttpPost]
        public void Create([FromBody] ProductCategory newProductCategory)
        {
            if (newProductCategory == null)
                throw new System.Exception();
            ProductCategoryDao productCategoryDAO = new ProductCategoryDao();
            productCategoryDAO.Create(newProductCategory);
        }

        /// <summary>
        /// Supprime une catégorie de produit
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="productId"></param>
        // DELETE api/<controller>/5
        [HttpDelete("{categoryId}/{productId}")]
        public void Delete(int categoryId, int productId)
        {
            ProductCategoryDao ProductCategory = new ProductCategoryDao();
            ProductCategory.Delete(categoryId, productId);
        }
    }
}
