﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WebApi.DataAccess;
using WebApi.ServicesMetiers;
using WebApi.Transport;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class OrderController : Controller
    {
        /// <summary>
        /// Recupere la liste de toutes les commandes 
        /// </summary>
        /// <returns></returns>
        // GET: api/order
        [HttpGet]
        public List<Order> GetAll()
        {
            OrderDao orderDAO = new OrderDao();
            return orderDAO.GetAll();

        }

        /// <summary>
        /// Pemet de recuperer une commande selon son identifiant
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/order/5
        [HttpGet("{id}")]
        public Order GetById(int id)
        {
            OrderDao orderDAO = new OrderDao();
            Order result= orderDAO.GetById(id);
            if (result != null)
            {
                OrderLineDao orderLineDao = new OrderLineDao();
                result.OrderLineList = orderLineDao.GetOrderLineByOrder(result.Identifiant);
            }
            return result;
        }

        /// <summary>
        /// Recupére la liste des commandes selon l'identifiant du magasin
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/order/GetById/5
        [HttpGet("GetByIdShop/{id}")]
        public List<OrderViewSeller> GetByIdShop(int id)
        {
            OrderDao orderDao = new OrderDao();
            OrderLineDao orderLineDao = new OrderLineDao(); 
            List<OrderViewSeller> orderList= orderDao.SelectOrderByIdShop(id);
            if (orderList != null && orderList.Count > 0)
            {
                List<OrderViewSeller> result = new List<OrderViewSeller>();

                foreach (OrderViewSeller order in orderList)
                {
                    order.OrderLineList = orderLineDao.GetOrderLineByOrder(order.Identifiant);
                    result.Add(order);
                }
                return result;
            }
            return null; 
        }

        /// <summary>
        /// Permet de créer une commande
        /// </summary>
        /// <param name="newOrder"></param>
        // POST api/order
        [HttpPost]
        public void Create([FromBody] Order newOrder)
        {
            if (newOrder == null)
                throw new System.Exception();
            CustomersServicesMetiers customersServicesMetiers = new CustomersServicesMetiers();
            customersServicesMetiers.CreerOrder(newOrder); 
        }

        /// <summary>
        /// Permet de mettre à jour une commande
        /// </summary>
        /// <param name="newOrder"></param>
        // PUT api/order
        [HttpPut]
        public void Update([FromBody]Order newOrder)
        {
            if (newOrder == null)
                throw new System.Exception();
            OrderDao orderDAO = new OrderDao();
            Order oldOrder = orderDAO.GetById(newOrder.Identifiant);

            if (oldOrder == null)
                throw new System.Exception();
            orderDAO.Update(newOrder);
        }

        /// <summary>
        /// Permet de mettre à jour le statut de la commande
        /// </summary>
        /// <param name="newOrder"></param>
        // PUT api/order
        [HttpPut("UpdateStatus")]
        public void UpdateStatus([FromBody]Order newOrder)
        {
            if (newOrder == null)
                throw new System.Exception();
            OrderDao orderDAO = new OrderDao();
            Order oldOrder = orderDAO.GetById(newOrder.Identifiant);

            if (oldOrder == null)
                throw new System.Exception();
            orderDAO.UpdateStatut(newOrder);
        }

        /// <summary>
        /// Supprime une commande 
        /// </summary>
        /// <param name="id"></param>
        // DELETE api/order/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            OrderDao orderDAO = new OrderDao();
            orderDAO.Delete(id);
        }

        /// <summary>
        /// Permet de mettre à jour la description de la commande
        /// </summary>
        /// <param name="newOrder"></param>
        // PUT api/order
        [HttpPut("UpdateDescription")]
        public void UpdateDescription([FromBody]Order newOrder)
        {
            if (newOrder == null)
                throw new System.Exception();
            OrderDao orderDAO = new OrderDao();
            Order oldOrder = orderDAO.GetById(newOrder.Identifiant);

            if (oldOrder == null)
                throw new System.Exception();
            orderDAO.UpdateDescription(newOrder);
        }
    }
}
