﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WebApi.DataAccess;
using WebApi.Transport;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class ShopCategoryController : Controller
    {
        /// <summary>
        /// Permet de récuperer la liste de toutes les categories des produits
        /// </summary>
        /// <returns></returns>
        // GET: api/ShopCategory
        [HttpGet]
        public List<ShopCategory> GetAll()
        {
            ShopCategoryDao ShopCategoryDao = new ShopCategoryDao();
            return ShopCategoryDao.GetAll();
        }

        /// <summary>
        /// permet de récuperer une categorie selon l'identifiant d'un produit
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/ShopCategory/GetByProductId/5
        [HttpGet("GetByShopId/{id}")]
        public List<ShopCategory> GetByShopId(int id)
        {
            ShopCategoryDao ShopCategoryDao = new ShopCategoryDao();
            return ShopCategoryDao.GetByShopId(id);
        }

        /// <summary>
        /// permet de recuperer une categorie selon son identifiant
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/ShopCategory/GetByCategoryId/5
        [HttpGet("GetByCategoryId/{id}")]
        public List<ShopCategory> GetByCategoryId(int id)
        {
            ShopCategoryDao ShopCategoryDao = new ShopCategoryDao();
            return ShopCategoryDao.GetByCategoryId(id);
        }

        /// <summary>
        /// Permet de créer une nouvelle categorie
        /// </summary>
        /// <param name="newShopCategory"></param>
        // POST api/<controller>
        [HttpPost]
        public void Create([FromBody] ShopCategory newShopCategory)
        {
            if (newShopCategory == null)
                throw new System.Exception();
            ShopCategoryDao ShopCategoryDAO = new ShopCategoryDao();
            ShopCategoryDAO.Create(newShopCategory);
        }

        /// <summary>
        /// Supprime une catégorie de produit
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="shopId"></param>
        // DELETE api/<controller>/5
        [HttpDelete("{categoryId}/{shopId}")]
        public void Delete(int categoryId, int shopId)
        {
            ShopCategoryDao ShopCategory = new ShopCategoryDao();
            ShopCategory.Delete(categoryId, shopId);
        }
    }
}
