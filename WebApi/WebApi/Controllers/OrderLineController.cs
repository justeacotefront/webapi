﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WebApi.DataAccess;
using WebApi.Transport;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class OrderLineController : Controller
    {
        /// <summary>
        /// Permet de récuperer la liste de toute les commandes 
        /// </summary>
        /// <returns></returns>
        // GET: api/orderline
        [HttpGet]
        public List<OrderLine> GetAll()
        {
            OrderLineDao orderLineDAO = new OrderLineDao();
            return orderLineDAO.GetAll();
        }

        /// <summary>
        /// Récupére une ligne de commande selon son identifiant
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/orderline/5
        [HttpGet("{id}")]
        public OrderLine GetById(int id)
        {
            OrderLineDao orderLineDAO = new OrderLineDao();
            return orderLineDAO.GetById(id);
        }

        /// <summary>
        /// Récupére une liste de ligne de commande selon l'identifiant de l'Order
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/orderline/5
        [HttpGet("GetOrderLineByIdOrder/{idOrder}")]
        public List<OrderLine> GetOrderLineByIdOrder(int idOrder)
        {
            OrderLineDao orderLineDAO = new OrderLineDao();
            return orderLineDAO.GetOrderLineByOrder(idOrder);
        }

        /// <summary>
        /// Permet de créer une ligne de commande
        /// </summary>
        /// <param name="newOrderLine"></param>
        // POST api/orderline
        [HttpPost]
        public void Create([FromBody] OrderLine newOrderLine)
        {
            if (newOrderLine == null)
                throw new System.Exception();
            OrderLineDao orderLineDAO = new OrderLineDao();
            orderLineDAO.Create(newOrderLine);
        }

        /// <summary>
        /// Permet de mettre à jour une ligne de commande
        /// </summary>
        /// <param name="newOrderLine"></param>
        // PUT api/orderline/5
        [HttpPut]
        public void Update([FromBody]OrderLine newOrderLine)
        {
            if (newOrderLine == null)
                throw new System.Exception();
            OrderLineDao orderLineDAO = new OrderLineDao();
            OrderLine oldOrderLine = orderLineDAO.GetById(newOrderLine.IdOrderLine);

            if (oldOrderLine == null)
                throw new System.Exception();
            orderLineDAO.Update(newOrderLine);
        }

        /// <summary>
        /// Supprime une ligne de commande
        /// </summary>
        /// <param name="id"></param>
        // DELETE api/orderline/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            OrderLineDao orderLineDAO = new OrderLineDao();
            orderLineDAO.Delete(id);
        }
    }
}
