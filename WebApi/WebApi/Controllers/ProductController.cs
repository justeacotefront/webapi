﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApi.DataAccess;
using WebApi.Transport;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        /// <summary>
        /// Récupére la liste des produits 
        /// </summary>
        /// <returns></returns>
        // GET: api/product
        [HttpGet]
        public List<Product> GetAll()
        {
            ProductDao productDAO = new ProductDao();
            List<Product> result = productDAO.GetAll();

            CommentDao commentDao = new CommentDao();
            if (result != null && result.Count > 0)
            {
                foreach (Product product in result)
                {
                    product.Commentaires = commentDao.GetByIdProduct(product.Identifiant);
                    if (product.IdImage.HasValue)
                    {
                        //On récupére l'image
                        PicturesDao pictureDao = new PicturesDao();
                        product.Image = pictureDao.GetById(product.IdImage.Value);
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Récupére la liste des 10 derniers
        /// </summary>
        /// <returns></returns>
        // GET: api/product
        [HttpGet("GetLastProduct")]
        public List<Product> GetLastProduct()
        {
            ProductDao productDAO = new ProductDao();
            List<Product> result = productDAO.GetLastProduct();
            if (result.Count() > 10)
            {
                result = (from p in result
                          select p).Take(10).ToList();
            }

            CommentDao commentDao = new CommentDao();
            foreach (Product product in result)
            {
                product.Commentaires = commentDao.GetByIdProduct(product.Identifiant);
                if (product.IdImage.HasValue)
                {
                    //On récupére l'image
                    PicturesDao pictureDao = new PicturesDao();
                    product.Image = pictureDao.GetById(product.IdImage.Value);
                }
            }
            return result;
        }

        /// <summary>
        /// Récupére un produit selon son identifiant
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/product/5
        [HttpGet("{id}")]
        public Product GetById(int id)
        {
            ProductDao productDAO = new ProductDao();
            Product result = productDAO.GetById(id);
            CommentDao commentDao = new CommentDao();
            result.Commentaires = commentDao.GetByIdProduct(result.Identifiant);
            if (result.IdImage.HasValue)
            {
                //On récupére l'image
                PicturesDao pictureDao = new PicturesDao();
                result.Image = pictureDao.GetById(result.IdImage.Value);
            }
            return result;
        }

        /// <summary>
        /// Recupére la liste des produit selon l'identifiant du magasin
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/product/GetById/5
        [HttpGet("GetByIdShop/{id}")]
        public List<Product> GetByIdShop(int id)
        {
            ProductDao productDAO = new ProductDao();
            List<Product> result = productDAO.GetByIdShop(id);
            CommentDao commentDao = new CommentDao();
            foreach (Product product in result)
            {
                product.Commentaires = commentDao.GetByIdProduct(product.Identifiant);
                if (product.IdImage.HasValue)
                {
                    //On récupére l'image
                    PicturesDao pictureDao = new PicturesDao();
                    product.Image = pictureDao.GetById(product.IdImage.Value);
                }
            }
            return result;
        }

        /// <summary>
        /// Crée un nouveau produit
        /// </summary>
        /// <param name="newProduct"></param>
        // POST api/product
        [HttpPost]
        public void Create([FromBody]Product newProduct)
        {
            if (newProduct == null)
                throw new System.Exception();
            ProductDao productDao = new ProductDao();
            productDao.Create(newProduct);
        }

        /// <summary>
        /// Met à jour un produit
        /// </summary>
        /// <param name="newProduct"></param>
        // PUT api/<controller>
        [HttpPut]
        public void Update([FromBody]Product newProduct)
        {
            if (newProduct == null)
                throw new System.Exception();
            ProductDao productDAO = new ProductDao();
            Product oldProduct = productDAO.GetById(newProduct.Identifiant);

            if (oldProduct == null)
                throw new System.Exception();
            productDAO.Update(newProduct);
        }

        /// <summary>
        /// Supprimer un produit
        /// </summary>
        /// <param name="id"></param>
        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            ProductDao productDAO = new ProductDao();
            Product produit = productDAO.GetById(id);
            //TODO si on supprime un produit on supprime l'image qu'il contient 

            if (produit == null)
                throw new System.Exception();
            productDAO.Delete(id);
        }

        /// <summary>
        /// Crée un nouvelle image lié à un produit
        /// </summary>
        /// <param name="picture"></param>
        // POST api/product
        [HttpPost("AddPicture/{idProduct}")]
        public void AddPicture(int idProduct, [FromBody]Picture picture)
        {

            try
            {
                //On verifie que notre objet image n'est pas null
                if (picture == null)
                    throw new System.Exception();
                //On verifie que l'id correspond bien à un produit
                ProductDao productDao = new ProductDao();
                Product produit = productDao.GetById(idProduct);
                if (produit == null)
                    throw new System.Exception();
                //On crée l'image et o récupére la clé primaire de l'objet
                PicturesDao pictureDao = new PicturesDao();
                int? idPicture = pictureDao.CreatePicture(picture);
                if (idPicture.HasValue)
                {
                    //Ajout du nouvelle id de l'image (Voir base de donnée)
                    produit.IdImage = idPicture.Value;
                    //On met à jour le produit avec le nouvelle id de l'image
                    productDao.Update(produit);
                }
                else {
                    throw new System.Exception();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }
        }

        /// <summary>
        /// Permet de mettre à jour le chemin de l'image du produit
        /// </summary>
        /// <param name="idProduct"></param>
        /// <param name="picturePath"></param>
        // get api/product/UpdatePicture
        [HttpGet("UpdatePicture/{idProduct}/{picturePath}")]
        public void UpdatePicture(int idProduct, string picturePath)
        {
            ProductDao productDAO = new ProductDao();
            Product oldProduct = productDAO.GetById(idProduct);
            if (oldProduct == null)
                throw new System.Exception();
            productDAO.UpdatePicturePath(idProduct, picturePath);
        }
    }
}
