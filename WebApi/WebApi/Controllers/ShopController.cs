﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApi.DataAccess;
using WebApi.Transport;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class ShopController : Controller
    {
        /// <summary>
        /// Permet de récupérer la liste des magasins
        /// </summary>
        /// <returns></returns>
        // GET: api/shop
        [HttpGet]
        public List<Shop> GetAll()
        {
            ShopDao shopDAO = new ShopDao();
            List<Shop> shopList = shopDAO.GetAll();
            if (shopList != null && shopList.Count > 0)
            {
                foreach (Shop shop in shopList)
                {
                    if (shop.IdImage.HasValue)
                    {
                        //On récupére l'image
                        PicturesDao pictureDao = new PicturesDao();
                        shop.Image = pictureDao.GetById(shop.IdImage.Value);
                    }

                }
            }
            return shopList;
        }

        // <summary>
        /// Permet de récupérer la liste des magasins avec les favoris renseignés
        /// </summary>
        /// <returns></returns>
        // GET: api/shop
        [HttpGet("GetAllByIdCustomer/{idCustomer}")]
        public List<Shop> GetAllByIdCustomer(int? idCustomer)
        {
            ShopDao shopDAO = new ShopDao();
            List<Favoris> favoris = null;
            if (idCustomer.HasValue)
            {
                FavorisDao favorisDao = new FavorisDao();
                favoris = new List<Favoris>(); 
                favoris = favorisDao.GetByIdCustomer(idCustomer.Value);
            }
           
            List<Shop> shopList = shopDAO.GetAll();
            if (shopList != null && shopList.Count > 0)
            {
                foreach (Shop shop in shopList)
                {
                    //if (shop.IdImage.HasValue)
                    //{
                    //    //On récupére l'image
                    //    PicturesDao pictureDao = new PicturesDao();
                    //    shop.Image = pictureDao.GetById(shop.IdImage.Value);
                    //}
                    if (favoris != null && favoris.Count > 0)
                    {
                        bool exists = favoris.Any(x => x.ShopId == shop.Identifiant);
                        if (exists)
                        {
                            shop.Favoris = true; 
                        }
                    }
                }
            }
            return shopList;
        }

        /// <summary>
        /// Permet de récupérer un magasin selon son identifiant
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/shop/5
        [HttpGet("{id}")]
        public Shop GetById(int id)
        {
            ShopDao shopDAO = new ShopDao();
            Shop shop = shopDAO.GetById(id);
            if (shop.IdImage.HasValue)
            {
                //On récupére l'image
                PicturesDao pictureDao = new PicturesDao();
                shop.Image = pictureDao.GetById(shop.IdImage.Value);
            }
            return shop;
        }

        /// <summary>
        /// Permet de créer un nouveau magasin
        /// </summary>
        /// <param name="newShop"></param>
        // POST api/shop/5
        [HttpPost("{idSeller}")]
        public void Create(int idSeller, [FromBody]Shop newShop)
        {
            if (newShop == null)
                throw new System.ArgumentNullException(); //newShop est null donc exception
            //On récupére les informations du vendeur qui veut créer le magasin
            SellerDao sellerDao = new SellerDao();
            Seller vendeur = sellerDao.GetById(idSeller);

            if (vendeur == null)
                throw new System.ArgumentNullException();// le vendeur n'existe pas donc exception

            ShopDao shopDAO = new ShopDao();
            shopDAO.Create(newShop);

            //On récupére le dernier magasin créer
            Shop lastShop = shopDAO.GetAll().OrderByDescending(s => s.CreationDate).FirstOrDefault();

            vendeur.ShopId = lastShop.Identifiant;
            sellerDao.Update(vendeur);

        }

        /// <summary>
        /// Permet de mettre à jour un magasin
        /// </summary>
        /// <param name="newShop"></param>
        // PUT api/shop
        [HttpPut]
        public void Update([FromBody]Shop newShop)
        {
            if (newShop == null)
                throw new System.Exception();
            ShopDao shopDAO = new ShopDao();
            Shop oldShop = shopDAO.GetById(newShop.Identifiant);
            if (oldShop == null)
                throw new System.Exception();
            shopDAO.Update(newShop);
        }

        /// <summary>
        /// Permet de supprimer un magasin selon son identifiant
        /// </summary>
        /// <param name="id"></param>
        // DELETE api/shop/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            ShopDao shopDAO = new ShopDao();
            Shop shop = shopDAO.GetById(id);
            if (shop == null)
                throw new System.Exception();
            shopDAO.Delete(shop.Identifiant);
        }

        /// <summary>
        /// Crée un nouvelle image lié à un magasin
        /// </summary>
        /// <param name="picture"></param>
        // POST api/product
        [HttpPost("AddPicture/{idCustomer}")]
        public void AddPicture(int idShop, [FromBody]Picture picture)
        {

            try
            {
                //On verifie que notre objet image n'est pas null
                if (picture == null)
                    throw new System.Exception();
                //On verifie que l'id correspond bien à un client
                ShopDao shopDAO = new ShopDao();
                Shop magasin = shopDAO.GetById(idShop);
                if (magasin == null)
                    throw new System.Exception();
                //On crée l'image et on récupére la clé primaire de l'objet
                PicturesDao pictureDao = new PicturesDao();
                int? idPicture = pictureDao.CreatePicture(picture);
                if (idPicture.HasValue)
                {
                    //Ajout du nouvelle id de l'image (Voir base de donnée)
                    magasin.IdImage = idPicture.Value;
                    //On met à jour le magasin avec le nouvelle id de l'image
                    shopDAO.Update(magasin);
                }
                else
                {
                    throw new System.Exception();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }
        }

        /// <summary>
        ///  Permet de mettre à jour le chemin de l'image du magasin
        /// </summary>
        /// <param name="idShop"></param>
        /// <param name="picturePath"></param>
        // get api/shop/UpdatePicture
        [HttpGet("UpdatePicture/{idShop}/{picturePath}")]
        public void UpdatePicture(int idShop, string picturePath)
        {
            ShopDao shopDAO = new ShopDao();
            Shop oldShop = shopDAO.GetById(idShop);
            if (oldShop == null)
                throw new System.Exception();
            shopDAO.UpdatePicturePath(idShop, picturePath);
        }
    }
}
