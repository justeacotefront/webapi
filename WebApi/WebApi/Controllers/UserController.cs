﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WebApi.DataAccess;
using WebApi.Transport;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        /// <summary>
        /// Permet de récupérer un user par son identifiant et son mdp
        /// </summary>
        /// <param name="login"></param>
        /// <param name="mdp"></param>
        /// <returns></returns>
        [HttpGet("{login}/{mdp}")]
        public User GetUser(string  login, string mdp )
        {
            if(string.IsNullOrEmpty(login)|| string.IsNullOrEmpty(mdp))
                throw new System.Exception();
            UserDao userDao = new UserDao();
            return userDao.GetByLoginMdp(login, mdp); 
        }

        /// <summary>
        /// Permet de récupérer tout les utilisateur existant
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public List<User> GetUser()
        {
            UserDao userDao = new UserDao();
            return userDao.GetAll();
        }

    }
}
