﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WebApi.DataAccess;
using WebApi.Transport;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class FavorisController : Controller
    {

        /// <summary>
        /// Permet de récuperer la liste des magasins favoris selon l'identifiant du client
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/favoris/5
        [HttpGet("{customerId}")]
        public List<Shop> Get(int customerId)
        {
            FavorisDao favorisDao = new FavorisDao();
            ShopDao shopDao = new ShopDao(); 
            List<Favoris> favorisList = favorisDao.GetByIdCustomer(customerId);
            List<Shop> result = new List<Shop>();
            if (favorisList != null && favorisList.Count > 0)
            {
                foreach (Favoris favoris in favorisList)
                {
                    Shop shop = shopDao.GetById(favoris.ShopId);
                    shop.Favoris = true; 
                    result.Add(shop); 
                }

            }
            return result.Count == 0 ? null : result;
        }
        

        /// <summary>
        /// Permet de créer un favoris
        /// </summary>
        /// <param name="comment"></param>
        // POST api/favoris
        [HttpPost]
        public bool Create([FromBody]Favoris favoris)
        {
            if (favoris == null)
                throw new System.Exception();
            FavorisDao favorisDao = new FavorisDao();
            Favoris favorisExistant = favorisDao.GetByIdCustomerAndShop(favoris.ShopId, favoris.CustomerId);
            if (favorisExistant.ShopId !=0 && favorisExistant.CustomerId!= 0)
                return false; 
            return favorisDao.Create(favoris);
        }

       

        /// <summary>
        /// Permet de supprimer un favoris
        /// </summary>
        /// <param name="id"></param>
        // DELETE api/favoris/idShop/idCustomer
        [HttpDelete("{idShop}/{idCustomer}")]
        public bool Delete(int idShop, int idCustomer)
        {
            FavorisDao favorisDao = new FavorisDao();
            Favoris favorisExistant = favorisDao.GetByIdCustomerAndShop(idShop, idCustomer);
            if (favorisExistant.ShopId == 0 && favorisExistant.CustomerId == 0)
                return false;
            return favorisDao.Delete(idShop, idCustomer);
        }
    }
}
