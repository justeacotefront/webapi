﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WebApi.DataAccess;
using WebApi.Transport;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class SellerController : Controller
    {
        /// <summary>
        /// Récupére la liste de tout les vendeurs
        /// </summary>
        /// <returns></returns>
        // GET: api/seller
        [HttpGet]
        public List<Seller> GetAll()
        {
            SellerDao sellerDAO = new SellerDao();
            List<Seller> sellerList = sellerDAO.GetAll();
            if (sellerList != null && sellerList.Count > 0)
            {
                foreach (Seller seller in sellerList)
                {
                    if (seller.IdImage.HasValue)
                    {
                        //On récupére l'image
                        PicturesDao pictureDao = new PicturesDao();
                        seller.Image = pictureDao.GetById(seller.IdImage.Value);
                    }
                }
            }
            return  sellerList;
        }

        /// <summary>
        /// Récupére un vendeur selon son identifiant
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/seller/5
        [HttpGet("{id}")]
        public Seller GetById(int id)
        {
            SellerDao sellerDAO = new SellerDao();
            Seller seller = sellerDAO.GetById(id);
            if (seller.IdImage.HasValue)
            {
                //On récupére l'image
                PicturesDao pictureDao = new PicturesDao();
                seller.Image = pictureDao.GetById(seller.IdImage.Value);
            }
            return seller; 
        }

        /// <summary>
        /// Récupére une liste de vendeurs selon l'identifiant du magasin
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/seller/GetByIdShop/5
        [HttpGet("GetByIdShop/{id}")]
        public List<Seller> GetByIdShop(int id)
        {
            SellerDao sellerDAO = new SellerDao();
            List<Seller> sellerList = sellerDAO.GetByIdShop(id);
            foreach (Seller seller in sellerList)
            {
                if (seller.IdImage.HasValue)
                {
                    //On récupére l'image
                    PicturesDao pictureDao = new PicturesDao();
                    seller.Image = pictureDao.GetById(seller.IdImage.Value);
                }
            }
            return sellerList;
        }

        /// <summary>
        /// Crée un nouveau vendeur
        /// </summary>
        /// <param name="newSeller"></param>
        // POST api/seller
        [HttpPost]
        public void Create([FromBody]Seller newSeller)
        {
            if (newSeller == null)
                throw new System.Exception();
            SellerDao sellerDAO = new SellerDao();
            sellerDAO.Create(newSeller);
        }

        /// <summary>
        /// Met à jour un vendeur
        /// </summary>
        /// <param name="newSeller"></param>
        // PUT api/seller
        [HttpPut]
        public void Update([FromBody]Seller newSeller)
        {
            if (newSeller == null)
                throw new System.Exception();
            SellerDao sellerDAO = new SellerDao();
            Seller oldSeller = sellerDAO.GetById(newSeller.Identifiant);
            if (oldSeller == null)
                throw new System.Exception();
            sellerDAO.Update(newSeller);
        }

        /// <summary>
        /// Supprime un vendeur selon son identifiant
        /// </summary>
        /// <param name="id"></param>
        // DELETE api/seller/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            SellerDao sellerDAO = new SellerDao();
            Seller oldSeller = sellerDAO.GetById(id);
            if (oldSeller == null)
                throw new System.Exception();
            sellerDAO.Delete(id);
        }

        /// <summary>
        /// Met à jour l'email d'un vendeur
        /// </summary>
        /// <param name="email"></param>
        /// <param name="identifiant"></param>
        [HttpPut("UpdateEmail/{identifiant}")]
        public void UpdateEmail(int identifiant, [FromBody]string email)
        {
            SellerDao sellerDao = new SellerDao();
            Seller sellerExistant = sellerDao.GetById(identifiant);
            if (sellerExistant == null || string.IsNullOrEmpty(email))
            {
                throw new System.Exception();
            }
            sellerDao.UpdateEmail(identifiant, email);

        }

        /// <summary>
        /// Crée un nouvelle image lié à un vendeur
        /// </summary>
        /// <param name="picture"></param>
        // POST api/product
        [HttpPost("AddPicture/{idVendeur}")]
        public void AddPicture(int idVendeur, [FromBody]Picture picture)
        {

            try
            {
                //On verifie que notre objet image n'est pas null
                if (picture == null)
                    throw new System.Exception();
                //On verifie que l'id correspond bien à un vendeur
                SellerDao sellerDAO = new SellerDao();
                Seller vendeur=  sellerDAO.GetById(idVendeur);
                if (vendeur == null)
                    throw new System.Exception();
                //On crée l'image et on récupére la clé primaire de l'objet
                PicturesDao pictureDao = new PicturesDao();
                int? idPicture = pictureDao.CreatePicture(picture);
                if (idPicture.HasValue)
                {
                    //Ajout du nouvelle id de l'image (Voir base de donnée)
                    vendeur.IdImage = idPicture.Value;
                    //On met à jour le vendeur avec le nouvelle id de l'image
                    sellerDAO.Update(vendeur);
                }
                else
                {
                    throw new System.Exception();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }
        }

        /// <summary>
        ///  Permet de mettre à jour le chemin de l'image du vendeur
        /// </summary>
        /// <param name="idSeller"></param>
        /// <param name="picturePath"></param>
        // get api/shop/UpdatePicture
        [HttpGet("UpdatePicture/{idSeller}/{picturePath}")]
        public void UpdatePicture(int idSeller, string picturePath)
        {
            SellerDao sellerDAO = new SellerDao();
            Seller oldSeller = sellerDAO.GetById(idSeller);
            if (oldSeller == null)
                throw new System.Exception();
            sellerDAO.UpdatePicturePath(idSeller, picturePath);
        }

    }
}
