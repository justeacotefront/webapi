﻿using System.Collections.Generic;
using WebApi.Assembler;
using WebApi.Transport;

namespace WebApi.DataAccess
{
    public class CategoryDao : BaseDao<Category, CategoryAssembler>
    {
        public override bool Create(Category model)
        {
            string sqlQuery;
            if (model.Parent.HasValue)
            {
                sqlQuery = CategorySqlTools.SqlQuery_InsertCategory(model.Parent.Value, model.Description);
            }
            else
            {
                sqlQuery = CategorySqlTools.SqlQuery_InsertSimpleCategory(model.Description);
            }
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;

        }

        public override bool Delete(int id)
        {
            string sqlQuery = CategorySqlTools.SqlQuery_Delete(id);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public override List<Category> GetAll()
        {
            string sqlQuery = CategorySqlTools.SqlQuery_SelectAll();
            List<Category> result = ExecuteReadList(sqlQuery);
            return result;
        }

        public override Category GetById(int id)
        {
            string sqlQuery = CategorySqlTools.SqlQuery_SelectById(id);
            Category result = ExecuteReadOne(sqlQuery);
            return result;
        }

        public override bool Update(Category model)
        {
            string sqlQuery;
            if (model.Parent.HasValue)
            {
                sqlQuery = CategorySqlTools.SqlQuery_Update(model.Identifiant, model.Parent.Value, model.Description);
            }
            else
            {
                sqlQuery = CategorySqlTools.SqlQuery_UpdateSimple(model.Identifiant, model.Description);
            }
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

    }
}
