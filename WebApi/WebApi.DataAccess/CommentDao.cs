﻿using System;
using System.Collections.Generic;
using WebApi.Assembler;
using WebApi.Transport;

namespace WebApi.DataAccess
{
    public class CommentDao : BaseDao<Comment, CommentAssembler>
    {
        public override bool Create(Comment model)
        {
            string sqlQuery = CommentSqlTools.SqlQuery_Insert(model.CustomerId, model.ProductId, model.Titre, model.Description);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public override bool Delete(int id)
        {
            string sqlQuery = CommentSqlTools.SqlQuery_Delete(id);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public override List<Comment> GetAll()
        {
            string sqlQuery = CommentSqlTools.SqlQuery_SelectAll();
            List<Comment> result = ExecuteReadList(sqlQuery);
            return result;
        }

        public override Comment GetById(int id)
        {
            string sqlQuery = CommentSqlTools.SqlQuery_SelectById(id);
            Comment result = ExecuteReadOne(sqlQuery);
            return result;
        }

        public override bool Update(Comment model)
        {
            string sqlQuery = CommentSqlTools.SqlQuery_Update(model.Identifiant, model.CustomerId, model.ProductId, model.Titre, model.Description);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public  List<Comment> GetByIdProduct( int idProduct)
        {
            string sqlQuery = CommentSqlTools.SqlQuery_SelectByIdProduct(idProduct);
            List<Comment> result = ExecuteReadList(sqlQuery);
            return result;
        }
    }
}
