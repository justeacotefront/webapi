﻿using System.Collections.Generic;
using System.Linq;
using WebApi.Assembler;
using WebApi.Transport;

namespace WebApi.DataAccess
{
    public class OrderDao : BaseDao<Order, OrderAssembler>
    {
        public int? CreateOrder(Order model)
        {
            string sqlQuery = OrderSqlTools.SqlQuery_Insert(model.CustomerId, model.IdShop, model.RecoveryDate);
            return ExecuteNonQueryInsert(sqlQuery);
        }

        public override bool Create(Order model)
        {
            throw new System.NotImplementedException();
        }
        public override bool Delete(int id)
        {
            string sqlQuery = OrderSqlTools.SqlQuery_Delete(id);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public override List<Order> GetAll()
        {
            string sqlQuery = OrderSqlTools.SqlQuery_SelectAll();
            List<Order> result = ExecuteReadList(sqlQuery);
            return result;
        }


        public override Order GetById(int id)
        {
            string sqlQuery = OrderSqlTools.SqlQuery_SelectById(id);
            Order result = ExecuteReadOne(sqlQuery);
            return result;
        }

        public override bool Update(Order model)
        {
            string sqlQuery = OrderSqlTools.SqlQuery_Update(model.Identifiant, model.Status, model.CustomerId, model.IdShop,model.RecoveryDate, model.Description);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public void UpdateStatut(Order model)
        {
            string sqlQuery = OrderSqlTools.SqlQuery_Update_Status(model.Identifiant, model.Status);
            ExecuteNonQuery(sqlQuery);
        }

        public List<OrderViewSeller> SelectOrderByIdShop(int idShop)
        {
            string sqlQuery = OrderSqlTools.SqlQuery_SelectByIdShop(idShop);
            List<Order> result = ExecuteReadList(sqlQuery);
            List<OrderViewSeller> orders = null;
            if (result != null && result.Count() > 0)
            {
                orders = new List<OrderViewSeller>();
                foreach (Order order in result)
                {
                    CustomerDao customerDao = new CustomerDao();
                    Customer customer = customerDao.GetById(order.CustomerId);
                    OrderViewSeller orderViewSeller = new OrderViewSeller();
                    orderViewSeller.Creation = order.Creation;
                    orderViewSeller.Identifiant = order.Identifiant;
                    orderViewSeller.Modification = order.Modification;
                    orderViewSeller.Status = order.Status;
                    orderViewSeller.RecoveryDate = order.RecoveryDate;
                    if (customer != null)
                    {
                        orderViewSeller.Customer = new CustomerViewSeller();
                        orderViewSeller.Customer.Nom = customer.Nom;
                        orderViewSeller.Customer.Prenom = customer.Prenom;
                        orderViewSeller.Customer.Identifiant = customer.Identifiant;
                        orderViewSeller.Customer.Email = customer.Email;
                        orderViewSeller.Customer.Telephone = customer.Telephone;
                    }
                    else {
                        orderViewSeller.Customer = new CustomerViewSeller();
                        orderViewSeller.Customer.Identifiant = order.Identifiant; 
                    }
                    orders.Add(orderViewSeller);
                }

            }
            return orders; 
        }

        public  List<Order> GetByIdCustomer(int idCustomer)
        {
            string sqlQuery = OrderSqlTools.SqlQuery_SelectByIdCustomer(idCustomer);
            List<Order> result = ExecuteReadList(sqlQuery);
            return result;
        }

        public Order GetCustomerOrderById(int idCustomer, int idOrder)
        {
            string sqlQuery = OrderSqlTools.SqlQuery_SelectCustomerOrderByIdOrder(idCustomer, idOrder);
            Order result = ExecuteReadOne(sqlQuery);
            return result;
        }

        public void UpdateDescription(Order model)
        {
            string sqlQuery = OrderSqlTools.SqlQuery_Update_Description(model.Identifiant, model.Description);
            ExecuteNonQuery(sqlQuery);
        }
    }
}
