﻿using System.Collections.Generic;
using System.Linq;
using WebApi.Assembler;
using WebApi.Transport;

namespace WebApi.DataAccess
{
    public class OrderLineDao : BaseDao<OrderLine, OrderLineAssembler>
    {
        public override bool Create(OrderLine model)
        {
            string sqlQuery = OrderLineSqlTools.SqlQuery_Insert(model.IdOrder, model.IdProduct, model.Quantite, model.PrixHT, model.VAT);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public override bool Delete(int id)
        {
            string sqlQuery = OrderLineSqlTools.SqlQuery_Delete(id);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public override List<OrderLine> GetAll()
        {
            string sqlQuery = OrderLineSqlTools.SqlQuery_SelectAll();
            List<OrderLine> result = ExecuteReadList(sqlQuery);
            return result;
        }

        public override OrderLine GetById(int id)
        {
            string sqlQuery = OrderLineSqlTools.SqlQuery_SelectById(id);
            OrderLine result = ExecuteReadOne(sqlQuery);
            return result;
        }
        public List<OrderLine> GetOrderLineByOrder(int idOrder)
        {
            string sqlQuery = OrderLineSqlTools.SqlQuery_SelectByIdOrder(idOrder);
            List<OrderLine> result = ExecuteReadList(sqlQuery);
            List<OrderLine> orderLineList = null; 
            if (result != null && result.Count() > 0)
            {
                orderLineList = new List<OrderLine>();
                foreach (OrderLine orderLine in result)
                {
                    ProductDao productDao = new ProductDao();
                    orderLine.Product = productDao.GetById(orderLine.IdProduct);
                    orderLineList.Add(orderLine);
                }
                 
            }
            return orderLineList;
        }
        public override bool Update(OrderLine model)
        {
            string sqlQuery = OrderLineSqlTools.SqlQuery_Update(model.IdOrder, model.IdProduct, model.Quantite, model.PrixHT, model.VAT, model.IdOrderLine);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }
    }
}
