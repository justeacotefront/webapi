﻿using System.Collections.Generic;
using WebApi.Assembler;
using WebApi.Transport;

namespace WebApi.DataAccess
{
    public class CustomerDao : BaseDao<Customer, CustomerAssembler>
    {
        public override bool Create(Customer model)
        {
            string sqlQuery = CustomerSqlTools.SqlQuery_Insert(model.Prenom, model.Nom, model.Telephone, model.Email,
                model.Rue, model.CP, model.Ville, model.UserId, model.Identifiant, model.PicturePath);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public override bool Delete(int id)
        {
            string sqlQuery = CustomerSqlTools.SqlQuery_Delete(id);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public override List<Customer> GetAll()
        {
            string sqlQuery = CustomerSqlTools.SqlQuery_SelectAll();
            List<Customer> result = ExecuteReadList(sqlQuery);
            return result;
        }

        public override Customer GetById(int id)
        {
            string sqlQuery = CustomerSqlTools.SqlQuery_SelectById(id);
            Customer result = ExecuteReadOne(sqlQuery);
            return result;
        }

        public override bool Update(Customer model)
        {
            string sqlQuery = CustomerSqlTools.SqlQuery_Update(model.Prenom, model.Nom, model.Telephone, model.Email,
                model.Rue, model.CP, model.Ville, model.UserId, model.PicturePath, model.Identifiant);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public bool UpdateEmail(int idCustomer, string email)
        {
            string sqlQuery = CustomerSqlTools.SqlQuery_Update_Email(email, idCustomer);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public bool UpdateAccount(Customer model)
        {
            string sqlQuery = CustomerSqlTools.SqlQuery_Update_Account((model.Disabled.HasValue && model.Disabled.Value) ? 0 : 1, model.Identifiant);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public bool UpdatePicturePath(int idCustomer, string picturePath)
        {
            string sqlQuery = CustomerSqlTools.SqlQuery_InsertPicturePath(picturePath, idCustomer);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }
    }
}
