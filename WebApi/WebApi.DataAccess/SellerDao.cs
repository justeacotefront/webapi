﻿using System.Collections.Generic;
using WebApi.Assembler;
using WebApi.Transport;

namespace WebApi.DataAccess
{
    public class SellerDao : BaseDao<Seller, SellerAssembler>
    {
        public override bool Create(Seller model)
        {
            string sqlQuery = SellerSqlTools.SqlQuery_Insert(model.ShopId, model.Prenom, model.Nom,
                model.Telephone, model.Email, model.Function, model.CreatedBy, model.UserId, model.Identifiant, model.PicturePath);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public override bool Delete(int id)
        {
            string sqlQuery = SellerSqlTools.SqlQuery_Delete(id);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public override List<Seller> GetAll()
        {
            string sqlQuery = SellerSqlTools.SqlQuery_SelectAll();
            List<Seller> result = ExecuteReadList(sqlQuery);
            return result;
        }

        public List<Seller> GetByIdShop(int id)
        {
            string sqlQuery = SellerSqlTools.SqlQuery_SelectByIdShop(id);
            List<Seller> result = ExecuteReadList(sqlQuery);
            return result;
        }

        public override Seller GetById(int id)
        {
            string sqlQuery = SellerSqlTools.SqlQuery_SelectById(id);
            Seller result = ExecuteReadOne(sqlQuery);
            return result;
        }

        public override bool Update(Seller model)
        {
            string sqlQuery = SellerSqlTools.SqlQuery_Update(model.Prenom, model.Nom,
                model.Telephone, model.Email, model.Function, model.CreatedBy, model.UserId, model.PicturePath, model.Identifiant);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public void UpdateEmail(int idSeller, string email)
        {
            string sqlQuery = SellerSqlTools.SqlQuery_Update_Email(email, idSeller);
            ExecuteNonQuery(sqlQuery);
        }

        public bool UpdatePicturePath(int idSeller, string picturePath)
        {
            string sqlQuery = SellerSqlTools.SqlQuery_InsertPicturePath(picturePath, idSeller);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }
    }
}
