﻿using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Text;
using WebApi.Assembler;
using WebApi.Transport;

namespace WebApi.DataAccess
{
    public class PicturesDao : BaseDao<Picture, PicturesAssembler>
    {
        public override bool Create(Picture model)
        {
            string sqlQuery = PicturesSqlTools.SqlQuery_Insert();
            int rows = ExecuteNonQueryPicture(sqlQuery, model.Blob);
            return rows > 0 ? true : false;
        }

        public override bool Delete(int id)
        {
            string sqlQuery = PicturesSqlTools.SqlQuery_Delete(id);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public override List<Picture> GetAll()
        {
            string sqlQuery = PicturesSqlTools.SqlQuery_SelectAll();
            List<Picture> result = ExecuteReadList(sqlQuery);
            return result;
        }

        public override Picture GetById(int id)
        {
            string sqlQuery = PicturesSqlTools.SqlQuery_SelectById(id);
            Picture result = ExecuteReadOne(sqlQuery);
            return result;
        }

        public override bool Update(Picture model)
        {
            string sqlQuery = PicturesSqlTools.SqlQuery_Update(model.Identifiant);
            int rows = ExecuteNonQueryPicture(sqlQuery, model.Blob);
            return rows > 0 ? true : false;
        }

        public int? CreatePicture(Picture model)
        {
            string sqlQuery = PicturesSqlTools.SqlQuery_InsertAndReturnPK();
            return ExecuteNonQueryInsertPicture(sqlQuery, model.Blob);
        }
    }
}
