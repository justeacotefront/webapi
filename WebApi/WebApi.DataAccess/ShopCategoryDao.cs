﻿using System.Collections.Generic;
using WebApi.Assembler;
using WebApi.Transport;

namespace WebApi.DataAccess
{
    public class ShopCategoryDao : BaseDao<ShopCategory, ShopCategoryAssembler>
    {
        public override bool Create(ShopCategory model)
        {
            string sqlQuery = ShopCategorySqlTools.SqlQuery_Insert(model.CategoryId, model.ShopId);
            int rows =ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }


        public  void Delete(int categoryId, int ShopId)
        {
            string sqlQuery = ShopCategorySqlTools.SqlQuery_Delete(categoryId, ShopId);
            ExecuteNonQuery(sqlQuery);
        }

        public List<ShopCategory>  GetByCategoryId(int categoryId)
        {
            string sqlQuery = ShopCategorySqlTools.SqlQuery_SelectByIdCategory(categoryId);
            List<ShopCategory> result = ExecuteReadList(sqlQuery);
            return result;
        }

        public List<ShopCategory> GetByShopId(int ShopId)
        {
            string sqlQuery = ShopCategorySqlTools.SqlQuery_SelectByIdProduct(ShopId);
            List<ShopCategory> result = ExecuteReadList(sqlQuery);
            return result;
        }
        public override List<ShopCategory> GetAll()
        {
            string sqlQuery = ShopCategorySqlTools.SqlQuery_SelectAll();
            List<ShopCategory> result = ExecuteReadList(sqlQuery);
            return result;
        }

        public override bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }
        public override ShopCategory GetById(int id)
        {
            throw new System.NotImplementedException();
        }

        public override bool Update(ShopCategory model)
        {
            throw new System.NotImplementedException();
        }
    }
}
