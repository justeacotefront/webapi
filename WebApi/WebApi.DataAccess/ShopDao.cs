﻿using System.Collections.Generic;
using WebApi.Assembler;
using WebApi.Transport;

namespace WebApi.DataAccess
{
    public class ShopDao : BaseDao<Shop, ShopAssembler>
    {
        public override bool Create(Shop model)
        {
            string sqlQuery = ShopSqlTools.SqlQuery_Insert(model.Nom, model.Description, model.Type, model.Rue, model.CodePostale, model.Ville,
                model.CreationShop, model.Createur, model.StatutLegal, model.NombreEmploye, model.TurnOverRange1, model.TurnOverRange2, model.PicturePath);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public override bool Delete(int id)
        {
            string sqlQuery = ShopSqlTools.SqlQuery_Delete(id);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public override List<Shop> GetAll()
        {
            string sqlQuery = ShopSqlTools.SqlQuery_SelectAll();
            List<Shop> result = ExecuteReadList(sqlQuery);
            return result;
        }

        public override Shop GetById(int id)
        {
            string sqlQuery = ShopSqlTools.SqlQuery_SelectById(id);
            Shop result = ExecuteReadOne(sqlQuery);
            return result;
        }

        public override bool Update(Shop model)
        {
            string sqlQuery = ShopSqlTools.SqlQuery_Update(model.Nom, model.Description, model.Type, model.Rue, model.CodePostale, model.Ville,
                model.CreationShop, model.Createur, model.StatutLegal, model.NombreEmploye, model.TurnOverRange1, model.TurnOverRange2, model.PicturePath, model.Identifiant);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public  bool UpdatePicturePath(int idShop, string picturePath)
        {
            string sqlQuery = ShopSqlTools.SqlQuery_InsertPicturePath(picturePath, idShop);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }
    }
}
