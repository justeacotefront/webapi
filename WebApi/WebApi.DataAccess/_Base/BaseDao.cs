﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using WebApi.Assembler;

namespace WebApi.DataAccess
{
    public abstract class BaseDao<T_Model, T_Assembler>
       where T_Model : class, new()
       where T_Assembler : BaseAssembler<T_Model>, new()
    {
        protected T_Assembler assembler;
        public const string _connectionString = "User Id=APP_DEV;Password=appDev2018!;Data Source=54.38.230.175/orcldev";

        public BaseDao()
        {
            assembler = new T_Assembler();
        }

        public abstract List<T_Model> GetAll();
        public abstract T_Model GetById(int id);
        public abstract bool Create(T_Model model);
        public abstract bool Update(T_Model model);
        public abstract bool Delete(int id);

        protected T_Model ExecuteReadOne(string query)
        {
            using (var _db = new OracleConnection(_connectionString))
            {
                _db.Open();
                OracleCommand cmd = new OracleCommand(query, _db);
                cmd.CommandType = CommandType.Text;
                OracleDataReader dr = cmd.ExecuteReader();
                dr.Read();
                T_Model result = assembler.ConvertItem(dr);
                _db.Close();
                return result;
            }
        }

        protected List<T_Model> ExecuteReadList(string query)
        {
            List<T_Model> result = null;

            using (var _db = new OracleConnection(_connectionString))
            {
                try
                {
                    OracleCommand cmd = new OracleCommand(query, _db);
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection.Open();
                    OracleDataReader dr = cmd.ExecuteReader();
                    result = assembler.ConvertList(dr);
                    //'close the connection
                    cmd.Connection.Close();
                    //'close the connection if it is still open
                    if (_db.State == System.Data.ConnectionState.Open)
                    {
                        _db.Close();
                    }
                }
                catch (OracleException ex)
                {
                    Console.WriteLine("Record is not inserted into the database table.");
                    Console.WriteLine("Exception Message: " + ex.Message);
                    Console.WriteLine("Exception Source: " + ex.Source);
                }
                return result;

            }
        }

        protected int ExecuteNonQuery(string query)
        {
            using (var _db = new OracleConnection(_connectionString))
            {
                _db.Open();
                OracleCommand cmd = new OracleCommand(query, _db);
                cmd.CommandType = CommandType.Text;
                int rows = cmd.ExecuteNonQuery();
                _db.Close();
                return rows;
            }
        }

        protected int ExecuteNonQueryPicture(string query, byte[] blob)
        {
            using (var _db = new OracleConnection(_connectionString))
            {
                _db.Open();
                OracleCommand cmnd;
                //insert the byte as oracle parameter of type blob

                OracleParameter blobParameter = new OracleParameter();
                blobParameter.OracleDbType = OracleDbType.Blob;
                blobParameter.ParameterName = "BlobParameter";
                blobParameter.Value = blob;

                cmnd = new OracleCommand(query, _db);
                cmnd.Parameters.Add(blobParameter);
                int rows = cmnd.ExecuteNonQuery();

                cmnd.Dispose();

                _db.Close();
                _db.Dispose();
                return rows;
            }
        }

        protected int? ExecuteNonQueryInsertPicture(string query, byte[] blob)
        {
            using (var _db = new OracleConnection(_connectionString))
            {
                _db.Open();
                OracleCommand cmd = new OracleCommand(query, _db);
                cmd.CommandType = CommandType.Text;

                // Remove any previously set Parameters
                cmd.Parameters.Clear();
                //Ajout du blob
                OracleParameter blobParameter = new OracleParameter();
                blobParameter.OracleDbType = OracleDbType.Blob;
                blobParameter.ParameterName = "BlobParameter";
                blobParameter.Value = blob;
                cmd.Parameters.Add(blobParameter);

                //Récupération de la primary key
                cmd.Parameters.Add(new OracleParameter
                {
                    ParameterName = ":id",
                    OracleDbType = OracleDbType.Int32,
                    Direction = ParameterDirection.Output
                });
                int rows = cmd.ExecuteNonQuery();

                _db.Close();

                if (cmd.Parameters[":id"] != null)
                    return Convert.ToInt32(cmd.Parameters[":id"].Value.ToString());
                else
                    return null;
            }
        }

        protected int? ExecuteNonQueryInsert(string query)
        {
            using (var _db = new OracleConnection(_connectionString))
            {
                _db.Open();
                OracleCommand cmd = new OracleCommand(query, _db);
                cmd.CommandType = CommandType.Text;

                // Remove any previously set Parameters
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new OracleParameter
                {
                    ParameterName = ":id",
                    OracleDbType = OracleDbType.Int32,
                    Direction = ParameterDirection.Output
                });
                int rows = cmd.ExecuteNonQuery();

                _db.Close();

                if (cmd.Parameters[":id"] != null)
                    return Convert.ToInt32(cmd.Parameters[":id"].Value.ToString());
                else
                    return null;
            }
        }

    }
}
