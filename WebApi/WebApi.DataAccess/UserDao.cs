﻿using System;
using System.Collections.Generic;
using WebApi.Assembler;
using WebApi.Transport;
using System.Linq; 

namespace WebApi.DataAccess
{
    public class UserDao : BaseDao<User, UserAssembler>
    {

        public override List<User> GetAll()
        {
            string sqlQuery = UserSqlTools.SqlQuery_SelectAll();
            List<User> result = ExecuteReadList(sqlQuery);
            return result;
        }

        public User GetByLoginMdp(string login, string mdp)
        {
            string sqlQuery = UserSqlTools.SqlQuery_SelectByLoginMdp(login, mdp);
            List<User> result = ExecuteReadList(sqlQuery);
            return result.FirstOrDefault();
        }

        public override bool Create(User model)
        {
            throw new NotImplementedException();
        }

        public override bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override User GetById(int id)
        {
            throw new NotImplementedException();
        }

        public override bool Update(User model)
        {
            throw new NotImplementedException();
        }
    }
}
