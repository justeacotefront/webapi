﻿using System.Collections.Generic;
using WebApi.Assembler;
using WebApi.Transport;


namespace WebApi.DataAccess
{
    public class ProductDao : BaseDao<Product, ProductAssembler>
    {
        public override bool Create(Product model)
        {
            string sqlQuery = ProductSqlTools.SqlQuery_Insert(model.ShopId, model.Name, model.Description
                ,model.Weight, model.WeightUnit, model.Lenght,model.LenghtUnit,model.Depth, model.DepthUnit,model.Width,
                model.WidthUnit,model.Height, model.HeightUnit,model.Price, model.Currency,model.Inventory, model.Origin, model.PicturePath);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public override bool Delete(int id)
        {
            string sqlQuery = ProductSqlTools.SqlQuery_Delete(id);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public override List<Product> GetAll()
        {
            string sqlQuery = ProductSqlTools.SqlQuery_SelectAll();
            List<Product> result = ExecuteReadList(sqlQuery);
            return result;
        }

        public override Product GetById(int id)
        {
            string sqlQuery = ProductSqlTools.SqlQuery_SelectById(id);
            Product result = ExecuteReadOne(sqlQuery);
            return result;
        }

        public  List<Product> GetByIdShop(int id)
        {
            string sqlQuery = ProductSqlTools.SqlQuery_SelectByIdShop(id);
            List<Product> result = ExecuteReadList(sqlQuery);
            return result;
        }

        public override bool Update(Product model)
        {
            string sqlQuery = ProductSqlTools.SqlQuery_Update(model.ShopId, model.Name, model.Description
                , model.Weight, model.WeightUnit, model.Lenght, model.LenghtUnit, model.Depth, model.DepthUnit, model.Width,
                model.WidthUnit, model.Height, model.HeightUnit, model.Price, model.Currency, model.Inventory, model.Origin,model.PicturePath, model.Identifiant);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public  List<Product> GetLastProduct()
        {
            string sqlQuery = ProductSqlTools.SqlQuery_SelectLastProduct();
            List<Product> result = ExecuteReadList(sqlQuery);
            return result;
        }

        public bool UpdatePicturePath(int idPicture, string picturePath)
        {
            string sqlQuery = ProductSqlTools.SqlQuery_InsertPicturePath(picturePath, idPicture);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }
    }
}
