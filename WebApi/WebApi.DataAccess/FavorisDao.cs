﻿using System.Collections.Generic;
using WebApi.Assembler;
using WebApi.Transport;

namespace WebApi.DataAccess
{
    public class FavorisDao : BaseDao<Favoris, FavorisAssembler>
    {
        public override bool Create(Favoris model)
        {
            string sqlQuery = FavorisSqlTools.SqlQuery_Insert(model.ShopId, model.CustomerId);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }

        public bool Delete(int shopId, int customerId) {
            string sqlQuery = FavorisSqlTools.SqlQuery_Delete(shopId, customerId);
            int rows = ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }
        public List<Favoris> GetByIdCustomer(int customerId)
        {
            string sqlQuery = FavorisSqlTools.SqlQuery_SelectByIdCustomer(customerId);
            List<Favoris> result = ExecuteReadList(sqlQuery);
            return result;
        }

        public Favoris GetByIdCustomerAndShop(int shopId, int customerId)
        {
            string sqlQuery = FavorisSqlTools.SqlQuery_SelectByIdCustomerAndShop(shopId, customerId);
            Favoris result = ExecuteReadOne(sqlQuery);
            return result;
        }

        public override bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public override List<Favoris> GetAll()
        {
            throw new System.NotImplementedException();
        }

        public override bool Update(Favoris model)
        {
            throw new System.NotImplementedException();
        }

        public override Favoris GetById(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}
