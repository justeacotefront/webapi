﻿using System.Collections.Generic;
using WebApi.Assembler;
using WebApi.Transport;

namespace WebApi.DataAccess
{
    public class ProductCategoryDao : BaseDao<ProductCategory, ProductCategoryAssembler>
    {
        public override bool Create(ProductCategory model)
        {
            string sqlQuery = ProductCategorySqlTools.SqlQuery_Insert(model.CategoryId, model.ProductId);
            int rows =ExecuteNonQuery(sqlQuery);
            return rows > 0 ? true : false;
        }


        public  void Delete(int categoryId, int productId)
        {
            string sqlQuery = ProductCategorySqlTools.SqlQuery_Delete(categoryId, productId);
            ExecuteNonQuery(sqlQuery);
        }

        public List<ProductCategory>  GetByCategoryId(int categoryId)
        {
            string sqlQuery = ProductCategorySqlTools.SqlQuery_SelectByIdCategory(categoryId);
            List<ProductCategory> result = ExecuteReadList(sqlQuery);
            return result;
        }

        public List<ProductCategory> GetByProductId(int productId)
        {
            string sqlQuery = ProductCategorySqlTools.SqlQuery_SelectByIdProduct(productId);
            List<ProductCategory> result = ExecuteReadList(sqlQuery);
            return result;
        }
        public override List<ProductCategory> GetAll()
        {
            string sqlQuery = ProductCategorySqlTools.SqlQuery_SelectAll();
            List<ProductCategory> result = ExecuteReadList(sqlQuery);
            return result;
        }

        public override bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }
        public override ProductCategory GetById(int id)
        {
            throw new System.NotImplementedException();
        }

        public override bool Update(ProductCategory model)
        {
            throw new System.NotImplementedException();
        }
    }
}
