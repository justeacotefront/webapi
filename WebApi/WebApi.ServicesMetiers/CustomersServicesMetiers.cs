﻿using System;
using System.Collections.Generic;
using WebApi.DataAccess;
using WebApi.Transport;

namespace WebApi.ServicesMetiers
{
    public class CustomersServicesMetiers
    {
        /// <summary>
        /// Récupere un customer par son identifiant
        /// </summary>
        /// <param name="idCustommer"></param>
        /// <returns></returns>
        public Customer GetCustomerById(int idCustommer)
        {
            if (idCustommer == 0)
                throw new ArgumentNullException("idCustommer");
            CustomerDao customerDao = new CustomerDao();
            return customerDao.GetById(idCustommer);
        }

        /// <summary>
        /// Crée un compte Customer
        /// </summary>
        /// <param name="newCustomer"></param>
        /// <returns></returns>
        public bool CreerCompteCustomer(Customer newCustomer)
        {
            if (newCustomer == null)
                throw new ArgumentNullException("newCustomer");

            CustomerDao customerDao = new CustomerDao();
            return customerDao.Create(newCustomer);

        }

        /// <summary>
        /// Met à jour les informations du customer
        /// </summary>
        /// <param name="newCustomer"></param>
        /// <returns></returns>
        public bool UpdateInfosCustomer(Customer newCustomer)
        {
            if (newCustomer.Identifiant == 0)
                throw new ArgumentNullException("newCustomer.Identifiant");

            CustomerDao customerDao = new CustomerDao();
            Customer customerExistant = customerDao.GetById(newCustomer.Identifiant);
            if (customerExistant == null)
                throw new ArgumentNullException("customerExistant");

            return customerDao.Update(newCustomer);
        }

        /// <summary>
        /// Modifie statut compte customer
        /// Activation/Désactivation du compte
        /// </summary>
        /// <param name="idCustomer"></param>
        /// <returns></returns>
        public bool UpdateStatusAccount(int idCustomer)
        {
            if (idCustomer == 0)
                throw new ArgumentNullException("idCustomer");

            CustomerDao customerDao = new CustomerDao();
            Customer customer = customerDao.GetById(idCustomer);

            if (customer == null)
                throw new ArgumentNullException("IdCustomer Introuvable en Base de données");

            return customerDao.UpdateAccount(customer);
        }

        /// <summary>
        /// Permet de récuperer tout les commandes passer par un utilisateur
        /// </summary>
        /// <param name="idCustomer"></param>
        /// <returns></returns>
        public List<Order> GetAllOrders(int idCustomer)
        {
            if (idCustomer == 0)
                throw new ArgumentNullException("idCustomer");

            OrderDao orderDAO = new OrderDao();
            List<Order> orders= orderDAO.GetByIdCustomer(idCustomer);
            if (orders != null && orders.Count > 0) {
                ShopDao shopDao = new ShopDao();
                foreach (Order commande in orders) {
                    if (commande.IdShop.HasValue)
                    {
                        Shop magasin = shopDao.GetById(commande.IdShop.Value);
                        commande.NomMagasin = magasin.Nom;
                    }
                }

            }
            return orders; 

        }

        /// <summary>
        /// Permet de créer une commande
        /// </summary>
        /// <param name="idCustomer"></param>
        /// <param name="newOrder"></param>
        /// <returns></returns>
        public bool CreerOrder(Order newOrder)
        {
            if (newOrder == null)
                throw new ArgumentNullException("newOrder");

            OrderDao orderDAO = new OrderDao();
            int? idOrder = orderDAO.CreateOrder(newOrder);
            if (idOrder.HasValue)
            {
                OrderLineDao orderLineDao = new OrderLineDao();
                if (newOrder.OrderLineList != null && newOrder.OrderLineList.Count > 0)
                {
                    foreach (OrderLine element in newOrder.OrderLineList)
                    {
                        element.IdOrder = idOrder.Value;
                        orderLineDao.Create(element);
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Ajout d'une image pour le client
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="picture"></param>
        /// <returns></returns>
        public bool AjoutImage(Customer customer, Picture picture) {

            PicturesDao pictureDao = new PicturesDao();
            int? idPicture = pictureDao.CreatePicture(picture);
            if (idPicture.HasValue)
            {
                //Ajout du nouvelle id de l'image (Voir base de donnée)
                customer.IdImage = idPicture.Value;
                //On met à jour le client avec le nouvelle id de l'image
                return UpdateInfosCustomer(customer);
                
            }
            else
            {
                return false;
            }
            
        }

  

    }
}
