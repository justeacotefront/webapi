﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApi.Transport
{
    public class ShopCategory
    {
        public int CategoryId { get; set; }
        public int ShopId { get; set; }
    }
}
