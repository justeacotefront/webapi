﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Transport
{
    public class User
    {
        public int Identifiant { get; set; }
        public string Login { get; set; }
        public string MDP { get; set; }
        public int? NombreLogin { get; set; }
        public DateTime? LastLogin { get; set; }
        public DateTime? DateCreation { get; set; }
        public DateTime? DateModification { get; set; }

    }
}
