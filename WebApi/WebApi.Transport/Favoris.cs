﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApi.Transport
{
    public class Favoris
    {
        public int ShopId { get; set; }
        public int CustomerId { get; set; }
        public DateTime Creation { get; set; }
    }
}
