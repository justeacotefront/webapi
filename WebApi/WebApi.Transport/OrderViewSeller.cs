﻿using System;
using System.Collections.Generic;

namespace WebApi.Transport
{
    public class OrderViewSeller
    {
        public int Identifiant { get; set; }
        public CustomerViewSeller Customer { get; set; }
        public DateTime Creation { get; set; }
        public DateTime Modification { get; set; }
        public string Status { get; set; }
        public DateTime? RecoveryDate { get; set; }
        public List<OrderLine> OrderLineList { get; set; }
        public string Description { get; set; }
    }
}
