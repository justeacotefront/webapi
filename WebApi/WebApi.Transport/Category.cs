﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Transport
{
    public class Category
    {
        public int? Parent { get; set; }
        public int Identifiant { get; set; }
        public string Description { get; set; }
        public DateTime Creation { get; set; }
        public DateTime? Modification { get; set; }
    }
}
