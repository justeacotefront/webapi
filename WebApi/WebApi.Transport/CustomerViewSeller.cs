﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApi.Transport
{
    public class CustomerViewSeller
    {
        public int Identifiant { get; set; }
        public string Prenom { get; set; }
        public string Nom { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
    }
}
