﻿using System;
using System.Collections.Generic;

namespace WebApi.Transport
{
    public class Order
    {
        public int Identifiant { get; set; }
        public int CustomerId { get; set; }
        public DateTime Creation { get; set; }
        public DateTime Modification { get; set; }
        public string Status { get; set; }
        public int? IdShop { get; set; }
        public DateTime? RecoveryDate { get; set; }
        public List<OrderLine> OrderLineList { get; set; }
        public string Description { get; set; }
        public string NomMagasin { get; set; }
    }
}
