﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Transport
{
    public class Seller
    {
        public int Identifiant { get; set; }
        public int ShopId { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string Function { get; set; }
        public DateTime Creation { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? Modification { get; set; }
        public int? UserId { get; set; }
        public int? IdImage { get; set; }
        public Picture Image { get; set; }
        public string PicturePath { get; set; }
    }
}
