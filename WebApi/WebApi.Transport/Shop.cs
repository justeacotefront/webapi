﻿using System;

namespace WebApi.Transport
{
    public class Shop
    {
        public int Identifiant { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Rue { get; set; }
        public int? CodePostale { get; set; }
        public string Ville { get; set; }
        public DateTime? CreationShop { get; set; }
        public string Createur { get; set; }
        public string StatutLegal { get; set; }
        public int? NombreEmploye { get; set; }
        public int? TurnOverRange1 { get; set; }
        public int? TurnOverRange2 { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? Modification { get; set; }
        public int? IdImage { get; set; }
        public Picture Image { get; set; }
        public bool Favoris { get; set; } = false;
        public string PicturePath { get; set; }

    }
}
