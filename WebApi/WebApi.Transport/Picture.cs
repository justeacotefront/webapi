﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApi.Transport
{
    public class Picture
    {
        public int Identifiant { get; set; }
        public Byte[] Blob { get; set; }
    }
}
