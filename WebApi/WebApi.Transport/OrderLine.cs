﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Transport
{
    public class OrderLine
    {
        public int IdOrderLine { get; set; }
        public int IdOrder { get; set; }
        public int IdProduct { get; set; }
        public Product Product { get; set; }
        public int? Quantite { get; set; }
        public float? PrixHT { get; set; }
        public int? VAT { get; set; }
        public DateTime Creation { get; set; }
        public DateTime? Modification { get; set; }

    }
}
