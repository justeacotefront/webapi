﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Transport
{
    public class Comment
    {
        public int Identifiant { get; set; }
        public int CustomerId { get; set; }
        public int ProductId { get; set; }
        public string Titre { get; set; }
        public string Description { get; set; }
        public DateTime Creation { get; set; }
        public DateTime Modification { get; set; }
    }
}
